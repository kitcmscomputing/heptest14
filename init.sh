#!/bin/bash

MYDIR=$(dirname $BASH_SOURCE)

SCRIPT_DIR=
SAMPLE_DIR="/local/scratch/hdd/mfischer/heptest/samples/"
STAGE_DIR="/local/scratch/hdd/mfischer/heptest/staging/"

CMS5_SCRIPT_DIR="mfischer@ekpcms5:/portal/ekpcms5/home/mfischer/hpda/benchmarks/rootTest"
CMS5_SAMPLE_DIR="mfischer@ekpcms5:/storage/5/mfischer/heptest/samples/"

NAF2_SCRIPT_DIR="mfischer@nafhh-cms02.desy.de:/afs/desy.de/user/m/mfischer/sonas/hpda/heptest/scripts"
NAF2_SAMPLE_DIR="mfischer@nafhh-cms02.desy.de:/afs/desy.de/user/m/mfischer/sonas/hpda/heptest/staging"

EV77_SCRIPT_DIR="condor@192.168.122.112:/home/condor/heptest/scripts"
EV77_SAMPLE_DIR="condor@192.168.122.112:/home/condor/heptest/staging"

RSYNC_ARGS="-vauh"

# where to get logs from (glob)
RemoteLogs=(
"mfischer@ekpcms5:/storage/5/mfischer/heptest/logs/*.out"
"mfischer@nafhh-cms02.desy.de:/afs/desy.de/user/m/mfischer/sonas/hpda/heptest/logs/*.out"
)
# condor@192.168.122.112:/home/condor/heptest/logs/*.out

LocalLogDir="/local/scratch/hdd/mfischer/heptest/logs/"

ht.pack_core () {
	(
		echo "Packing ROOT core files"
		thisdir=$(pwd)
		cd core
		tar -czvf $thisdir/staging/rootTestCore.tgz *
	)
}
ht.pack_samples () {
	(
		echo "Packing Samples"
		for SAMPLE in $(find $SAMPLE_DIR -maxdepth 1 -mindepth 1 -type d -printf '%f\n')
		do
			(
				cd $SAMPLE_DIR/$SAMPLE
				SAMPLENAME=rootTestSample
				SAMPLENAME+=${SAMPLE#data*}
				SAMPLENAME+=".tar"
				echo $SAMPLENAME
				tar -cf $STAGE_DIR/$SAMPLENAME *
			)
		done
	)
}

ht.sync.cms5 () {
	(
		echo "O Synching to EKP CMS5"
		# sync scripts
		echo "  [] Synching Scripts..."
		rsync $RSYNC_ARGS $MYDIR/scripts/*.sh $CMS5_SCRIPT_DIR
		# sync macro
		echo "  [] Synching Core..."
		rsync $RSYNC_ARGS $MYDIR/staging/rootTestCore.tgz $CMS5_SCRIPT_DIR
		# sync samples
		echo "  [] Synching Samples..."
		rsync $RSYNC_ARGS $MYDIR/staging/*.tar $CMS5_SAMPLE_DIR
	)
}
ht.sync.naf2 () {
	(
		echo "O Synching to NAF 2.0"
		# sync scripts
		echo "  [] Synching Scripts..."
		rsync $RSYNC_ARGS $MYDIR/scripts/*.sh $NAF2_SCRIPT_DIR
		# sync macro
		echo "  [] Synching Core..."
		rsync $RSYNC_ARGS $MYDIR/staging/rootTestCore.tgz $NAF2_SCRIPT_DIR
		# sync samples
		echo "  [] Synching Samples..."
		rsync $RSYNC_ARGS $MYDIR/staging/*.tar $NAF2_SAMPLE_DIR
	)
}
ht.sync.ekpvmlx77 () {
	(
		echo "O Synching to EVM 77"
		# sync scripts
		echo "  [] Synching Scripts..."
		rsync $RSYNC_ARGS $MYDIR/scripts/*.sh $EV77_SCRIPT_DIR
		# sync macro
		echo "  [] Synching Core..."
		rsync $RSYNC_ARGS $MYDIR/staging/rootTestCore.tgz $EV77_SCRIPT_DIR
		# sync samples
		echo "  [] Synching Samples..."
		rsync $RSYNC_ARGS $MYDIR/staging/*.tar $EV77_SAMPLE_DIR
	)
}

ht.getlogs () {
	(
		echo "O Fetching remote logs..."
		for LogLocation in ${RemoteLogs[@]}
		do
			echo "[] Fetching from ${LogLocation%:*} ..."
			rsync $RSYNC_ARGS ${LogLocation} $LocalLogDir
		done
	)
}

# announce vailable functions
echo "Initiated HPDA benchmarking control environment"
echo "Available functions:"
grep '() {' $BASH_SOURCE | grep -v "grep" | awk '{print $1}'
