//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Feb  6 19:30:02 2012 by ROOT version 5.30/01
// from TTree Events/Events
// found on file: muons2010/kappa_2011-09-21_MZ_428_Tutorial_Mu_2010A_Apr21thRR_195.root
//////////////////////////////////////////////////////////

#ifndef MuBenchSelector_h
#define MuBenchSelector_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
   const Int_t kMaxmuons = 46;
// define buffer lenght run statistics
   const Int_t tbuflen=300;

class MuBenchSelector : public TSelector {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain

// user defined variables may come here:
   UInt_t fNumberOfEvents; TDatime tBegin, tNow;
   Float_t tbuf[tbuflen];

// Declaration of leaf types
 //KEventMetadata  *KEventMetadata;
   ULong64_t       bitsL1;
   ULong64_t       bitsHLT;
   UInt_t          bitsUserFlags;
   UInt_t          nEvent;
   UInt_t          nLumi;
   UInt_t          nRun;
   Int_t           nBX;
   Int_t           muons_;
   Float_t         muons_p4_fCoordinates_fPt[kMaxmuons];   //[muons_]
   Float_t         muons_p4_fCoordinates_fEta[kMaxmuons];   //[muons_]
   Float_t         muons_p4_fCoordinates_fPhi[kMaxmuons];   //[muons_]
   Float_t         muons_p4_fCoordinates_fM[kMaxmuons];   //[muons_]
   Char_t          muons_charge[kMaxmuons];   //[muons_]
   Float_t         muons_track_p4_fCoordinates_fPt[kMaxmuons];   //[muons_]
   Float_t         muons_track_p4_fCoordinates_fEta[kMaxmuons];   //[muons_]
   Float_t         muons_track_p4_fCoordinates_fPhi[kMaxmuons];   //[muons_]
   Float_t         muons_track_p4_fCoordinates_fM[kMaxmuons];   //[muons_]
   Float_t         muons_track_ref_fCoordinates_fX[kMaxmuons];   //[muons_]
   Float_t         muons_track_ref_fCoordinates_fY[kMaxmuons];   //[muons_]
   Float_t         muons_track_ref_fCoordinates_fZ[kMaxmuons];   //[muons_]
   Char_t          muons_track_charge[kMaxmuons];   //[muons_]
   Float_t         muons_track_chi2[kMaxmuons];   //[muons_]
   Float_t         muons_track_nDOF[kMaxmuons];   //[muons_]
   Float_t         muons_track_errPt[kMaxmuons];   //[muons_]
   Float_t         muons_track_errEta[kMaxmuons];   //[muons_]
   Float_t         muons_track_errPhi[kMaxmuons];   //[muons_]
   Float_t         muons_track_errDxy[kMaxmuons];   //[muons_]
   Float_t         muons_track_errDz[kMaxmuons];   //[muons_]
   UShort_t        muons_track_nPixelLayers[kMaxmuons];   //[muons_]
   UShort_t        muons_track_nStripLayers[kMaxmuons];   //[muons_]
   UShort_t        muons_track_nValidPixelHits[kMaxmuons];   //[muons_]
   UShort_t        muons_track_nValidStripHits[kMaxmuons];   //[muons_]
   UShort_t        muons_track_nValidMuonHits[kMaxmuons];   //[muons_]
   UShort_t        muons_track_nLostMuonHits[kMaxmuons];   //[muons_]
   UShort_t        muons_track_nBadMuonHits[kMaxmuons];   //[muons_]
   UShort_t        muons_track_nValidHits[kMaxmuons];   //[muons_]
   UShort_t        muons_track_nLostHits[kMaxmuons];   //[muons_]
   Int_t           muons_track_quality[kMaxmuons];   //[muons_]
   Float_t         muons_track_sumPtIso03[kMaxmuons];   //[muons_]
   Float_t         muons_track_trackIso03[kMaxmuons];   //[muons_]
   Float_t         muons_globalTrack_p4_fCoordinates_fPt[kMaxmuons];   //[muons_]
   Float_t         muons_globalTrack_p4_fCoordinates_fEta[kMaxmuons];   //[muons_]
   Float_t         muons_globalTrack_p4_fCoordinates_fPhi[kMaxmuons];   //[muons_]
   Float_t         muons_globalTrack_p4_fCoordinates_fM[kMaxmuons];   //[muons_]
   Float_t         muons_globalTrack_ref_fCoordinates_fX[kMaxmuons];   //[muons_]
   Float_t         muons_globalTrack_ref_fCoordinates_fY[kMaxmuons];   //[muons_]
   Float_t         muons_globalTrack_ref_fCoordinates_fZ[kMaxmuons];   //[muons_]
   Char_t          muons_globalTrack_charge[kMaxmuons];   //[muons_]
   Float_t         muons_globalTrack_chi2[kMaxmuons];   //[muons_]
   Float_t         muons_globalTrack_nDOF[kMaxmuons];   //[muons_]
   Float_t         muons_globalTrack_errPt[kMaxmuons];   //[muons_]
   Float_t         muons_globalTrack_errEta[kMaxmuons];   //[muons_]
   Float_t         muons_globalTrack_errPhi[kMaxmuons];   //[muons_]
   Float_t         muons_globalTrack_errDxy[kMaxmuons];   //[muons_]
   Float_t         muons_globalTrack_errDz[kMaxmuons];   //[muons_]
   UShort_t        muons_globalTrack_nPixelLayers[kMaxmuons];   //[muons_]
   UShort_t        muons_globalTrack_nStripLayers[kMaxmuons];   //[muons_]
   UShort_t        muons_globalTrack_nValidPixelHits[kMaxmuons];   //[muons_]
   UShort_t        muons_globalTrack_nValidStripHits[kMaxmuons];   //[muons_]
   UShort_t        muons_globalTrack_nValidMuonHits[kMaxmuons];   //[muons_]
   UShort_t        muons_globalTrack_nLostMuonHits[kMaxmuons];   //[muons_]
   UShort_t        muons_globalTrack_nBadMuonHits[kMaxmuons];   //[muons_]
   UShort_t        muons_globalTrack_nValidHits[kMaxmuons];   //[muons_]
   UShort_t        muons_globalTrack_nLostHits[kMaxmuons];   //[muons_]
   Int_t           muons_globalTrack_quality[kMaxmuons];   //[muons_]
   Float_t         muons_globalTrack_sumPtIso03[kMaxmuons];   //[muons_]
   Float_t         muons_globalTrack_trackIso03[kMaxmuons];   //[muons_]
   Float_t         muons_innerTrack_p4_fCoordinates_fPt[kMaxmuons];   //[muons_]
   Float_t         muons_innerTrack_p4_fCoordinates_fEta[kMaxmuons];   //[muons_]
   Float_t         muons_innerTrack_p4_fCoordinates_fPhi[kMaxmuons];   //[muons_]
   Float_t         muons_innerTrack_p4_fCoordinates_fM[kMaxmuons];   //[muons_]
   Float_t         muons_innerTrack_ref_fCoordinates_fX[kMaxmuons];   //[muons_]
   Float_t         muons_innerTrack_ref_fCoordinates_fY[kMaxmuons];   //[muons_]
   Float_t         muons_innerTrack_ref_fCoordinates_fZ[kMaxmuons];   //[muons_]
   Char_t          muons_innerTrack_charge[kMaxmuons];   //[muons_]
   Float_t         muons_innerTrack_chi2[kMaxmuons];   //[muons_]
   Float_t         muons_innerTrack_nDOF[kMaxmuons];   //[muons_]
   Float_t         muons_innerTrack_errPt[kMaxmuons];   //[muons_]
   Float_t         muons_innerTrack_errEta[kMaxmuons];   //[muons_]
   Float_t         muons_innerTrack_errPhi[kMaxmuons];   //[muons_]
   Float_t         muons_innerTrack_errDxy[kMaxmuons];   //[muons_]
   Float_t         muons_innerTrack_errDz[kMaxmuons];   //[muons_]
   UShort_t        muons_innerTrack_nPixelLayers[kMaxmuons];   //[muons_]
   UShort_t        muons_innerTrack_nStripLayers[kMaxmuons];   //[muons_]
   UShort_t        muons_innerTrack_nValidPixelHits[kMaxmuons];   //[muons_]
   UShort_t        muons_innerTrack_nValidStripHits[kMaxmuons];   //[muons_]
   UShort_t        muons_innerTrack_nValidMuonHits[kMaxmuons];   //[muons_]
   UShort_t        muons_innerTrack_nLostMuonHits[kMaxmuons];   //[muons_]
   UShort_t        muons_innerTrack_nBadMuonHits[kMaxmuons];   //[muons_]
   UShort_t        muons_innerTrack_nValidHits[kMaxmuons];   //[muons_]
   UShort_t        muons_innerTrack_nLostHits[kMaxmuons];   //[muons_]
   Int_t           muons_innerTrack_quality[kMaxmuons];   //[muons_]
   Float_t         muons_innerTrack_sumPtIso03[kMaxmuons];   //[muons_]
   Float_t         muons_innerTrack_trackIso03[kMaxmuons];   //[muons_]
   Float_t         muons_outerTrack_p4_fCoordinates_fPt[kMaxmuons];   //[muons_]
   Float_t         muons_outerTrack_p4_fCoordinates_fEta[kMaxmuons];   //[muons_]
   Float_t         muons_outerTrack_p4_fCoordinates_fPhi[kMaxmuons];   //[muons_]
   Float_t         muons_outerTrack_p4_fCoordinates_fM[kMaxmuons];   //[muons_]
   Float_t         muons_outerTrack_ref_fCoordinates_fX[kMaxmuons];   //[muons_]
   Float_t         muons_outerTrack_ref_fCoordinates_fY[kMaxmuons];   //[muons_]
   Float_t         muons_outerTrack_ref_fCoordinates_fZ[kMaxmuons];   //[muons_]
   Char_t          muons_outerTrack_charge[kMaxmuons];   //[muons_]
   Float_t         muons_outerTrack_chi2[kMaxmuons];   //[muons_]
   Float_t         muons_outerTrack_nDOF[kMaxmuons];   //[muons_]
   Float_t         muons_outerTrack_errPt[kMaxmuons];   //[muons_]
   Float_t         muons_outerTrack_errEta[kMaxmuons];   //[muons_]
   Float_t         muons_outerTrack_errPhi[kMaxmuons];   //[muons_]
   Float_t         muons_outerTrack_errDxy[kMaxmuons];   //[muons_]
   Float_t         muons_outerTrack_errDz[kMaxmuons];   //[muons_]
   UShort_t        muons_outerTrack_nPixelLayers[kMaxmuons];   //[muons_]
   UShort_t        muons_outerTrack_nStripLayers[kMaxmuons];   //[muons_]
   UShort_t        muons_outerTrack_nValidPixelHits[kMaxmuons];   //[muons_]
   UShort_t        muons_outerTrack_nValidStripHits[kMaxmuons];   //[muons_]
   UShort_t        muons_outerTrack_nValidMuonHits[kMaxmuons];   //[muons_]
   UShort_t        muons_outerTrack_nLostMuonHits[kMaxmuons];   //[muons_]
   UShort_t        muons_outerTrack_nBadMuonHits[kMaxmuons];   //[muons_]
   UShort_t        muons_outerTrack_nValidHits[kMaxmuons];   //[muons_]
   UShort_t        muons_outerTrack_nLostHits[kMaxmuons];   //[muons_]
   Int_t           muons_outerTrack_quality[kMaxmuons];   //[muons_]
   Float_t         muons_outerTrack_sumPtIso03[kMaxmuons];   //[muons_]
   Float_t         muons_outerTrack_trackIso03[kMaxmuons];   //[muons_]
   UChar_t         muons_type[kMaxmuons];   //[muons_]
   Float_t         muons_sumPtIso03[kMaxmuons];   //[muons_]
   Float_t         muons_hcalIso03[kMaxmuons];   //[muons_]
   Float_t         muons_ecalIso03[kMaxmuons];   //[muons_]
   Float_t         muons_trackIso03[kMaxmuons];   //[muons_]
   Float_t         muons_pfIso04[kMaxmuons];   //[muons_]
   Float_t         muons_sumPtIso05[kMaxmuons];   //[muons_]
   Float_t         muons_hcalIso05[kMaxmuons];   //[muons_]
   Float_t         muons_ecalIso05[kMaxmuons];   //[muons_]
   Float_t         muons_trackIso05[kMaxmuons];   //[muons_]
   UInt_t          muons_isGoodMuon[kMaxmuons];   //[muons_]
   Float_t         muons_caloComp[kMaxmuons];   //[muons_]
   Float_t         muons_segComp[kMaxmuons];   //[muons_]
   Int_t           muons_numberOfChambers[kMaxmuons];   //[muons_]
   Int_t           muons_numberOfMatches[kMaxmuons];   //[muons_]
   ULong64_t       muons_hltMatch[kMaxmuons];   //[muons_]
   Float_t         muons_eta_propagated[kMaxmuons];   //[muons_]
   Float_t         muons_phi_propagated[kMaxmuons];   //[muons_]
 //KTrackSummary   *generalTracksSummary;
   UInt_t          nTracks;
   UInt_t          nTracksHQ;
 //KDataPFMET      *PFMET;
   Float_t         p4_fCoordinates_fPt;
   Float_t         p4_fCoordinates_fEta;
   Float_t         p4_fCoordinates_fPhi;
   Float_t         p4_fCoordinates_fM;
   Double_t        sumEt;
   Double_t        chargedEMEtFraction;
   Double_t        chargedHadEtFraction;
   Double_t        neutralEMEtFraction;
   Double_t        neutralHadEtFraction;
   Double_t        muonEtFraction;
   Double_t        type6EtFraction;
   Double_t        type7EtFraction;
 //KVertexSummary  *offlinePrimaryVerticesSummary;
   Float_t         pv_position_fCoordinates_fX;
   Float_t         pv_position_fCoordinates_fY;
   Float_t         pv_position_fCoordinates_fZ;
   Bool_t          pv_fake;
   UInt_t          pv_nTracks;
   Float_t         pv_chi2;
   Float_t         pv_nDOF;
   Double_t        pv_covariance_fRep_fArray[6];
   UInt_t          nVertices;
 //KVertexSummary  *offlinePrimaryVerticesWithBSSummary;
 //  Float_t         pv_position_fCoordinates_fX;
 //  Float_t         pv_position_fCoordinates_fY;
 //  Float_t         pv_position_fCoordinates_fZ;
 //  Bool_t          pv_fake;
 //  UInt_t          pv_nTracks;
 //  Float_t         pv_chi2;
 //  Float_t         pv_nDOF;
 //  Double_t        pv_covariance_fRep_fArray[6];
 //  UInt_t          nVertices;
 //KDataBeamSpot   *offlineBeamSpot;
   Float_t         position_fCoordinates_fX;
   Float_t         position_fCoordinates_fY;
   Float_t         position_fCoordinates_fZ;
   Char_t          type;
   Double_t        betaStar;
   Double_t        beamWidthX;
   Double_t        beamWidthY;
   Double_t        emittanceX;
   Double_t        emittanceY;
   Double_t        dxdz;
   Double_t        dydz;
   Double_t        sigmaZ;
   Double_t        covariance_fRep_fArray[28];
 //KJetArea        *KT6Area;
   Float_t         median;
   Float_t         sigma;

   // List of branches
   TBranch        *b_KEventMetadata_bitsL1;   //!
   TBranch        *b_KEventMetadata_bitsHLT;   //!
   TBranch        *b_KEventMetadata_bitsUserFlags;   //!
   TBranch        *b_KEventMetadata_nEvent;   //!
   TBranch        *b_KEventMetadata_nLumi;   //!
   TBranch        *b_KEventMetadata_nRun;   //!
   TBranch        *b_KEventMetadata_nBX;   //!
   TBranch        *b_muons_;   //!
   TBranch        *b_muons_p4_fCoordinates_fPt;   //!
   TBranch        *b_muons_p4_fCoordinates_fEta;   //!
   TBranch        *b_muons_p4_fCoordinates_fPhi;   //!
   TBranch        *b_muons_p4_fCoordinates_fM;   //!
   TBranch        *b_muons_charge;   //!
   TBranch        *b_muons_track_p4_fCoordinates_fPt;   //!
   TBranch        *b_muons_track_p4_fCoordinates_fEta;   //!
   TBranch        *b_muons_track_p4_fCoordinates_fPhi;   //!
   TBranch        *b_muons_track_p4_fCoordinates_fM;   //!
   TBranch        *b_muons_track_ref_fCoordinates_fX;   //!
   TBranch        *b_muons_track_ref_fCoordinates_fY;   //!
   TBranch        *b_muons_track_ref_fCoordinates_fZ;   //!
   TBranch        *b_muons_track_charge;   //!
   TBranch        *b_muons_track_chi2;   //!
   TBranch        *b_muons_track_nDOF;   //!
   TBranch        *b_muons_track_errPt;   //!
   TBranch        *b_muons_track_errEta;   //!
   TBranch        *b_muons_track_errPhi;   //!
   TBranch        *b_muons_track_errDxy;   //!
   TBranch        *b_muons_track_errDz;   //!
   TBranch        *b_muons_track_nPixelLayers;   //!
   TBranch        *b_muons_track_nStripLayers;   //!
   TBranch        *b_muons_track_nValidPixelHits;   //!
   TBranch        *b_muons_track_nValidStripHits;   //!
   TBranch        *b_muons_track_nValidMuonHits;   //!
   TBranch        *b_muons_track_nLostMuonHits;   //!
   TBranch        *b_muons_track_nBadMuonHits;   //!
   TBranch        *b_muons_track_nValidHits;   //!
   TBranch        *b_muons_track_nLostHits;   //!
   TBranch        *b_muons_track_quality;   //!
   TBranch        *b_muons_track_sumPtIso03;   //!
   TBranch        *b_muons_track_trackIso03;   //!
   TBranch        *b_muons_globalTrack_p4_fCoordinates_fPt;   //!
   TBranch        *b_muons_globalTrack_p4_fCoordinates_fEta;   //!
   TBranch        *b_muons_globalTrack_p4_fCoordinates_fPhi;   //!
   TBranch        *b_muons_globalTrack_p4_fCoordinates_fM;   //!
   TBranch        *b_muons_globalTrack_ref_fCoordinates_fX;   //!
   TBranch        *b_muons_globalTrack_ref_fCoordinates_fY;   //!
   TBranch        *b_muons_globalTrack_ref_fCoordinates_fZ;   //!
   TBranch        *b_muons_globalTrack_charge;   //!
   TBranch        *b_muons_globalTrack_chi2;   //!
   TBranch        *b_muons_globalTrack_nDOF;   //!
   TBranch        *b_muons_globalTrack_errPt;   //!
   TBranch        *b_muons_globalTrack_errEta;   //!
   TBranch        *b_muons_globalTrack_errPhi;   //!
   TBranch        *b_muons_globalTrack_errDxy;   //!
   TBranch        *b_muons_globalTrack_errDz;   //!
   TBranch        *b_muons_globalTrack_nPixelLayers;   //!
   TBranch        *b_muons_globalTrack_nStripLayers;   //!
   TBranch        *b_muons_globalTrack_nValidPixelHits;   //!
   TBranch        *b_muons_globalTrack_nValidStripHits;   //!
   TBranch        *b_muons_globalTrack_nValidMuonHits;   //!
   TBranch        *b_muons_globalTrack_nLostMuonHits;   //!
   TBranch        *b_muons_globalTrack_nBadMuonHits;   //!
   TBranch        *b_muons_globalTrack_nValidHits;   //!
   TBranch        *b_muons_globalTrack_nLostHits;   //!
   TBranch        *b_muons_globalTrack_quality;   //!
   TBranch        *b_muons_globalTrack_sumPtIso03;   //!
   TBranch        *b_muons_globalTrack_trackIso03;   //!
   TBranch        *b_muons_innerTrack_p4_fCoordinates_fPt;   //!
   TBranch        *b_muons_innerTrack_p4_fCoordinates_fEta;   //!
   TBranch        *b_muons_innerTrack_p4_fCoordinates_fPhi;   //!
   TBranch        *b_muons_innerTrack_p4_fCoordinates_fM;   //!
   TBranch        *b_muons_innerTrack_ref_fCoordinates_fX;   //!
   TBranch        *b_muons_innerTrack_ref_fCoordinates_fY;   //!
   TBranch        *b_muons_innerTrack_ref_fCoordinates_fZ;   //!
   TBranch        *b_muons_innerTrack_charge;   //!
   TBranch        *b_muons_innerTrack_chi2;   //!
   TBranch        *b_muons_innerTrack_nDOF;   //!
   TBranch        *b_muons_innerTrack_errPt;   //!
   TBranch        *b_muons_innerTrack_errEta;   //!
   TBranch        *b_muons_innerTrack_errPhi;   //!
   TBranch        *b_muons_innerTrack_errDxy;   //!
   TBranch        *b_muons_innerTrack_errDz;   //!
   TBranch        *b_muons_innerTrack_nPixelLayers;   //!
   TBranch        *b_muons_innerTrack_nStripLayers;   //!
   TBranch        *b_muons_innerTrack_nValidPixelHits;   //!
   TBranch        *b_muons_innerTrack_nValidStripHits;   //!
   TBranch        *b_muons_innerTrack_nValidMuonHits;   //!
   TBranch        *b_muons_innerTrack_nLostMuonHits;   //!
   TBranch        *b_muons_innerTrack_nBadMuonHits;   //!
   TBranch        *b_muons_innerTrack_nValidHits;   //!
   TBranch        *b_muons_innerTrack_nLostHits;   //!
   TBranch        *b_muons_innerTrack_quality;   //!
   TBranch        *b_muons_innerTrack_sumPtIso03;   //!
   TBranch        *b_muons_innerTrack_trackIso03;   //!
   TBranch        *b_muons_outerTrack_p4_fCoordinates_fPt;   //!
   TBranch        *b_muons_outerTrack_p4_fCoordinates_fEta;   //!
   TBranch        *b_muons_outerTrack_p4_fCoordinates_fPhi;   //!
   TBranch        *b_muons_outerTrack_p4_fCoordinates_fM;   //!
   TBranch        *b_muons_outerTrack_ref_fCoordinates_fX;   //!
   TBranch        *b_muons_outerTrack_ref_fCoordinates_fY;   //!
   TBranch        *b_muons_outerTrack_ref_fCoordinates_fZ;   //!
   TBranch        *b_muons_outerTrack_charge;   //!
   TBranch        *b_muons_outerTrack_chi2;   //!
   TBranch        *b_muons_outerTrack_nDOF;   //!
   TBranch        *b_muons_outerTrack_errPt;   //!
   TBranch        *b_muons_outerTrack_errEta;   //!
   TBranch        *b_muons_outerTrack_errPhi;   //!
   TBranch        *b_muons_outerTrack_errDxy;   //!
   TBranch        *b_muons_outerTrack_errDz;   //!
   TBranch        *b_muons_outerTrack_nPixelLayers;   //!
   TBranch        *b_muons_outerTrack_nStripLayers;   //!
   TBranch        *b_muons_outerTrack_nValidPixelHits;   //!
   TBranch        *b_muons_outerTrack_nValidStripHits;   //!
   TBranch        *b_muons_outerTrack_nValidMuonHits;   //!
   TBranch        *b_muons_outerTrack_nLostMuonHits;   //!
   TBranch        *b_muons_outerTrack_nBadMuonHits;   //!
   TBranch        *b_muons_outerTrack_nValidHits;   //!
   TBranch        *b_muons_outerTrack_nLostHits;   //!
   TBranch        *b_muons_outerTrack_quality;   //!
   TBranch        *b_muons_outerTrack_sumPtIso03;   //!
   TBranch        *b_muons_outerTrack_trackIso03;   //!
   TBranch        *b_muons_type;   //!
   TBranch        *b_muons_sumPtIso03;   //!
   TBranch        *b_muons_hcalIso03;   //!
   TBranch        *b_muons_ecalIso03;   //!
   TBranch        *b_muons_trackIso03;   //!
   TBranch        *b_muons_pfIso04;   //!
   TBranch        *b_muons_sumPtIso05;   //!
   TBranch        *b_muons_hcalIso05;   //!
   TBranch        *b_muons_ecalIso05;   //!
   TBranch        *b_muons_trackIso05;   //!
   TBranch        *b_muons_isGoodMuon;   //!
   TBranch        *b_muons_caloComp;   //!
   TBranch        *b_muons_segComp;   //!
   TBranch        *b_muons_numberOfChambers;   //!
   TBranch        *b_muons_numberOfMatches;   //!
   TBranch        *b_muons_hltMatch;   //!
   TBranch        *b_muons_eta_propagated;   //!
   TBranch        *b_muons_phi_propagated;   //!
   TBranch        *b_generalTracksSummary_nTracks;   //!
   TBranch        *b_generalTracksSummary_nTracksHQ;   //!
   TBranch        *b_PFMET_p4_fCoordinates_fPt;   //!
   TBranch        *b_PFMET_p4_fCoordinates_fEta;   //!
   TBranch        *b_PFMET_p4_fCoordinates_fPhi;   //!
   TBranch        *b_PFMET_p4_fCoordinates_fM;   //!
   TBranch        *b_PFMET_sumEt;   //!
   TBranch        *b_PFMET_chargedEMEtFraction;   //!
   TBranch        *b_PFMET_chargedHadEtFraction;   //!
   TBranch        *b_PFMET_neutralEMEtFraction;   //!
   TBranch        *b_PFMET_neutralHadEtFraction;   //!
   TBranch        *b_PFMET_muonEtFraction;   //!
   TBranch        *b_PFMET_type6EtFraction;   //!
   TBranch        *b_PFMET_type7EtFraction;   //!
   TBranch        *b_offlinePrimaryVerticesSummary_pv_position_fCoordinates_fX;   //!
   TBranch        *b_offlinePrimaryVerticesSummary_pv_position_fCoordinates_fY;   //!
   TBranch        *b_offlinePrimaryVerticesSummary_pv_position_fCoordinates_fZ;   //!
   TBranch        *b_offlinePrimaryVerticesSummary_pv_fake;   //!
   TBranch        *b_offlinePrimaryVerticesSummary_pv_nTracks;   //!
   TBranch        *b_offlinePrimaryVerticesSummary_pv_chi2;   //!
   TBranch        *b_offlinePrimaryVerticesSummary_pv_nDOF;   //!
   TBranch        *b_offlinePrimaryVerticesSummary_pv_covariance_fRep_fArray;   //!
   TBranch        *b_offlinePrimaryVerticesSummary_nVertices;   //!
   TBranch        *b_offlinePrimaryVerticesWithBSSummary_pv_position_fCoordinates_fX;   //!
   TBranch        *b_offlinePrimaryVerticesWithBSSummary_pv_position_fCoordinates_fY;   //!
   TBranch        *b_offlinePrimaryVerticesWithBSSummary_pv_position_fCoordinates_fZ;   //!
   TBranch        *b_offlinePrimaryVerticesWithBSSummary_pv_fake;   //!
   TBranch        *b_offlinePrimaryVerticesWithBSSummary_pv_nTracks;   //!
   TBranch        *b_offlinePrimaryVerticesWithBSSummary_pv_chi2;   //!
   TBranch        *b_offlinePrimaryVerticesWithBSSummary_pv_nDOF;   //!
   TBranch        *b_offlinePrimaryVerticesWithBSSummary_pv_covariance_fRep_fArray;   //!
   TBranch        *b_offlinePrimaryVerticesWithBSSummary_nVertices;   //!
   TBranch        *b_offlineBeamSpot_position_fCoordinates_fX;   //!
   TBranch        *b_offlineBeamSpot_position_fCoordinates_fY;   //!
   TBranch        *b_offlineBeamSpot_position_fCoordinates_fZ;   //!
   TBranch        *b_offlineBeamSpot_type;   //!
   TBranch        *b_offlineBeamSpot_betaStar;   //!
   TBranch        *b_offlineBeamSpot_beamWidthX;   //!
   TBranch        *b_offlineBeamSpot_beamWidthY;   //!
   TBranch        *b_offlineBeamSpot_emittanceX;   //!
   TBranch        *b_offlineBeamSpot_emittanceY;   //!
   TBranch        *b_offlineBeamSpot_dxdz;   //!
   TBranch        *b_offlineBeamSpot_dydz;   //!
   TBranch        *b_offlineBeamSpot_sigmaZ;   //!
   TBranch        *b_offlineBeamSpot_covariance_fRep_fArray;   //!
   TBranch        *b_KT6Area_median;   //!
   TBranch        *b_KT6Area_sigma;   //!

   MuBenchSelector(TTree * /*tree*/ =0) { }
   ~MuBenchSelector() {}
   Int_t   Version() const { return 2; }
   void    Begin(TTree *tree);
   void    SlaveBegin(TTree *tree);
   void    Init(TTree *tree);
   Bool_t  Notify();
   Bool_t  Process(Long64_t entry);
   Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   void    SetOption(const char *option) { fOption = option; }
   void    SetObject(TObject *obj) { fObject = obj; }
   void    SetInputList(TList *input) { fInput = input; }
   TList  *GetOutputList() const { return fOutput; }
   void    SlaveTerminate();
   void    Terminate();

   //ClassDef(MuBenchSelector,2);
};

#endif

#ifdef MuBenchSelector_cpp
// Only works with the token of the cpp file... (ROOT did it)
void MuBenchSelector::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // This is auto-generatred from ROOT

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("bitsL1", &bitsL1, &b_KEventMetadata_bitsL1);
   fChain->SetBranchAddress("bitsHLT", &bitsHLT, &b_KEventMetadata_bitsHLT);
   fChain->SetBranchAddress("bitsUserFlags", &bitsUserFlags, &b_KEventMetadata_bitsUserFlags);
   fChain->SetBranchAddress("nEvent", &nEvent, &b_KEventMetadata_nEvent);
   fChain->SetBranchAddress("nLumi", &nLumi, &b_KEventMetadata_nLumi);
   fChain->SetBranchAddress("nRun", &nRun, &b_KEventMetadata_nRun);
   fChain->SetBranchAddress("nBX", &nBX, &b_KEventMetadata_nBX);
   fChain->SetBranchAddress("muons", &muons_, &b_muons_);
   fChain->SetBranchAddress("muons.p4.fCoordinates.fPt", muons_p4_fCoordinates_fPt, &b_muons_p4_fCoordinates_fPt);
   fChain->SetBranchAddress("muons.p4.fCoordinates.fEta", muons_p4_fCoordinates_fEta, &b_muons_p4_fCoordinates_fEta);
   fChain->SetBranchAddress("muons.p4.fCoordinates.fPhi", muons_p4_fCoordinates_fPhi, &b_muons_p4_fCoordinates_fPhi);
   fChain->SetBranchAddress("muons.p4.fCoordinates.fM", muons_p4_fCoordinates_fM, &b_muons_p4_fCoordinates_fM);
   fChain->SetBranchAddress("muons.charge", muons_charge, &b_muons_charge);
   fChain->SetBranchAddress("muons.track.p4.fCoordinates.fPt", muons_track_p4_fCoordinates_fPt, &b_muons_track_p4_fCoordinates_fPt);
   fChain->SetBranchAddress("muons.track.p4.fCoordinates.fEta", muons_track_p4_fCoordinates_fEta, &b_muons_track_p4_fCoordinates_fEta);
   fChain->SetBranchAddress("muons.track.p4.fCoordinates.fPhi", muons_track_p4_fCoordinates_fPhi, &b_muons_track_p4_fCoordinates_fPhi);
   fChain->SetBranchAddress("muons.track.p4.fCoordinates.fM", muons_track_p4_fCoordinates_fM, &b_muons_track_p4_fCoordinates_fM);
   fChain->SetBranchAddress("muons.track.ref.fCoordinates.fX", muons_track_ref_fCoordinates_fX, &b_muons_track_ref_fCoordinates_fX);
   fChain->SetBranchAddress("muons.track.ref.fCoordinates.fY", muons_track_ref_fCoordinates_fY, &b_muons_track_ref_fCoordinates_fY);
   fChain->SetBranchAddress("muons.track.ref.fCoordinates.fZ", muons_track_ref_fCoordinates_fZ, &b_muons_track_ref_fCoordinates_fZ);
   fChain->SetBranchAddress("muons.track.charge", muons_track_charge, &b_muons_track_charge);
   fChain->SetBranchAddress("muons.track.chi2", muons_track_chi2, &b_muons_track_chi2);
   fChain->SetBranchAddress("muons.track.nDOF", muons_track_nDOF, &b_muons_track_nDOF);
   fChain->SetBranchAddress("muons.track.errPt", muons_track_errPt, &b_muons_track_errPt);
   fChain->SetBranchAddress("muons.track.errEta", muons_track_errEta, &b_muons_track_errEta);
   fChain->SetBranchAddress("muons.track.errPhi", muons_track_errPhi, &b_muons_track_errPhi);
   fChain->SetBranchAddress("muons.track.errDxy", muons_track_errDxy, &b_muons_track_errDxy);
   fChain->SetBranchAddress("muons.track.errDz", muons_track_errDz, &b_muons_track_errDz);
   fChain->SetBranchAddress("muons.track.nPixelLayers", muons_track_nPixelLayers, &b_muons_track_nPixelLayers);
   fChain->SetBranchAddress("muons.track.nStripLayers", muons_track_nStripLayers, &b_muons_track_nStripLayers);
   fChain->SetBranchAddress("muons.track.nValidPixelHits", muons_track_nValidPixelHits, &b_muons_track_nValidPixelHits);
   fChain->SetBranchAddress("muons.track.nValidStripHits", muons_track_nValidStripHits, &b_muons_track_nValidStripHits);
   fChain->SetBranchAddress("muons.track.nValidMuonHits", muons_track_nValidMuonHits, &b_muons_track_nValidMuonHits);
   fChain->SetBranchAddress("muons.track.nLostMuonHits", muons_track_nLostMuonHits, &b_muons_track_nLostMuonHits);
   fChain->SetBranchAddress("muons.track.nBadMuonHits", muons_track_nBadMuonHits, &b_muons_track_nBadMuonHits);
   fChain->SetBranchAddress("muons.track.nValidHits", muons_track_nValidHits, &b_muons_track_nValidHits);
   fChain->SetBranchAddress("muons.track.nLostHits", muons_track_nLostHits, &b_muons_track_nLostHits);
   fChain->SetBranchAddress("muons.track.quality", muons_track_quality, &b_muons_track_quality);
   fChain->SetBranchAddress("muons.track.sumPtIso03", muons_track_sumPtIso03, &b_muons_track_sumPtIso03);
   fChain->SetBranchAddress("muons.track.trackIso03", muons_track_trackIso03, &b_muons_track_trackIso03);
   fChain->SetBranchAddress("muons.globalTrack.p4.fCoordinates.fPt", muons_globalTrack_p4_fCoordinates_fPt, &b_muons_globalTrack_p4_fCoordinates_fPt);
   fChain->SetBranchAddress("muons.globalTrack.p4.fCoordinates.fEta", muons_globalTrack_p4_fCoordinates_fEta, &b_muons_globalTrack_p4_fCoordinates_fEta);
   fChain->SetBranchAddress("muons.globalTrack.p4.fCoordinates.fPhi", muons_globalTrack_p4_fCoordinates_fPhi, &b_muons_globalTrack_p4_fCoordinates_fPhi);
   fChain->SetBranchAddress("muons.globalTrack.p4.fCoordinates.fM", muons_globalTrack_p4_fCoordinates_fM, &b_muons_globalTrack_p4_fCoordinates_fM);
   fChain->SetBranchAddress("muons.globalTrack.ref.fCoordinates.fX", muons_globalTrack_ref_fCoordinates_fX, &b_muons_globalTrack_ref_fCoordinates_fX);
   fChain->SetBranchAddress("muons.globalTrack.ref.fCoordinates.fY", muons_globalTrack_ref_fCoordinates_fY, &b_muons_globalTrack_ref_fCoordinates_fY);
   fChain->SetBranchAddress("muons.globalTrack.ref.fCoordinates.fZ", muons_globalTrack_ref_fCoordinates_fZ, &b_muons_globalTrack_ref_fCoordinates_fZ);
   fChain->SetBranchAddress("muons.globalTrack.charge", muons_globalTrack_charge, &b_muons_globalTrack_charge);
   fChain->SetBranchAddress("muons.globalTrack.chi2", muons_globalTrack_chi2, &b_muons_globalTrack_chi2);
   fChain->SetBranchAddress("muons.globalTrack.nDOF", muons_globalTrack_nDOF, &b_muons_globalTrack_nDOF);
   fChain->SetBranchAddress("muons.globalTrack.errPt", muons_globalTrack_errPt, &b_muons_globalTrack_errPt);
   fChain->SetBranchAddress("muons.globalTrack.errEta", muons_globalTrack_errEta, &b_muons_globalTrack_errEta);
   fChain->SetBranchAddress("muons.globalTrack.errPhi", muons_globalTrack_errPhi, &b_muons_globalTrack_errPhi);
   fChain->SetBranchAddress("muons.globalTrack.errDxy", muons_globalTrack_errDxy, &b_muons_globalTrack_errDxy);
   fChain->SetBranchAddress("muons.globalTrack.errDz", muons_globalTrack_errDz, &b_muons_globalTrack_errDz);
   fChain->SetBranchAddress("muons.globalTrack.nPixelLayers", muons_globalTrack_nPixelLayers, &b_muons_globalTrack_nPixelLayers);
   fChain->SetBranchAddress("muons.globalTrack.nStripLayers", muons_globalTrack_nStripLayers, &b_muons_globalTrack_nStripLayers);
   fChain->SetBranchAddress("muons.globalTrack.nValidPixelHits", muons_globalTrack_nValidPixelHits, &b_muons_globalTrack_nValidPixelHits);
   fChain->SetBranchAddress("muons.globalTrack.nValidStripHits", muons_globalTrack_nValidStripHits, &b_muons_globalTrack_nValidStripHits);
   fChain->SetBranchAddress("muons.globalTrack.nValidMuonHits", muons_globalTrack_nValidMuonHits, &b_muons_globalTrack_nValidMuonHits);
   fChain->SetBranchAddress("muons.globalTrack.nLostMuonHits", muons_globalTrack_nLostMuonHits, &b_muons_globalTrack_nLostMuonHits);
   fChain->SetBranchAddress("muons.globalTrack.nBadMuonHits", muons_globalTrack_nBadMuonHits, &b_muons_globalTrack_nBadMuonHits);
   fChain->SetBranchAddress("muons.globalTrack.nValidHits", muons_globalTrack_nValidHits, &b_muons_globalTrack_nValidHits);
   fChain->SetBranchAddress("muons.globalTrack.nLostHits", muons_globalTrack_nLostHits, &b_muons_globalTrack_nLostHits);
   fChain->SetBranchAddress("muons.globalTrack.quality", muons_globalTrack_quality, &b_muons_globalTrack_quality);
   fChain->SetBranchAddress("muons.globalTrack.sumPtIso03", muons_globalTrack_sumPtIso03, &b_muons_globalTrack_sumPtIso03);
   fChain->SetBranchAddress("muons.globalTrack.trackIso03", muons_globalTrack_trackIso03, &b_muons_globalTrack_trackIso03);
   fChain->SetBranchAddress("muons.innerTrack.p4.fCoordinates.fPt", muons_innerTrack_p4_fCoordinates_fPt, &b_muons_innerTrack_p4_fCoordinates_fPt);
   fChain->SetBranchAddress("muons.innerTrack.p4.fCoordinates.fEta", muons_innerTrack_p4_fCoordinates_fEta, &b_muons_innerTrack_p4_fCoordinates_fEta);
   fChain->SetBranchAddress("muons.innerTrack.p4.fCoordinates.fPhi", muons_innerTrack_p4_fCoordinates_fPhi, &b_muons_innerTrack_p4_fCoordinates_fPhi);
   fChain->SetBranchAddress("muons.innerTrack.p4.fCoordinates.fM", muons_innerTrack_p4_fCoordinates_fM, &b_muons_innerTrack_p4_fCoordinates_fM);
   fChain->SetBranchAddress("muons.innerTrack.ref.fCoordinates.fX", muons_innerTrack_ref_fCoordinates_fX, &b_muons_innerTrack_ref_fCoordinates_fX);
   fChain->SetBranchAddress("muons.innerTrack.ref.fCoordinates.fY", muons_innerTrack_ref_fCoordinates_fY, &b_muons_innerTrack_ref_fCoordinates_fY);
   fChain->SetBranchAddress("muons.innerTrack.ref.fCoordinates.fZ", muons_innerTrack_ref_fCoordinates_fZ, &b_muons_innerTrack_ref_fCoordinates_fZ);
   fChain->SetBranchAddress("muons.innerTrack.charge", muons_innerTrack_charge, &b_muons_innerTrack_charge);
   fChain->SetBranchAddress("muons.innerTrack.chi2", muons_innerTrack_chi2, &b_muons_innerTrack_chi2);
   fChain->SetBranchAddress("muons.innerTrack.nDOF", muons_innerTrack_nDOF, &b_muons_innerTrack_nDOF);
   fChain->SetBranchAddress("muons.innerTrack.errPt", muons_innerTrack_errPt, &b_muons_innerTrack_errPt);
   fChain->SetBranchAddress("muons.innerTrack.errEta", muons_innerTrack_errEta, &b_muons_innerTrack_errEta);
   fChain->SetBranchAddress("muons.innerTrack.errPhi", muons_innerTrack_errPhi, &b_muons_innerTrack_errPhi);
   fChain->SetBranchAddress("muons.innerTrack.errDxy", muons_innerTrack_errDxy, &b_muons_innerTrack_errDxy);
   fChain->SetBranchAddress("muons.innerTrack.errDz", muons_innerTrack_errDz, &b_muons_innerTrack_errDz);
   fChain->SetBranchAddress("muons.innerTrack.nPixelLayers", muons_innerTrack_nPixelLayers, &b_muons_innerTrack_nPixelLayers);
   fChain->SetBranchAddress("muons.innerTrack.nStripLayers", muons_innerTrack_nStripLayers, &b_muons_innerTrack_nStripLayers);
   fChain->SetBranchAddress("muons.innerTrack.nValidPixelHits", muons_innerTrack_nValidPixelHits, &b_muons_innerTrack_nValidPixelHits);
   fChain->SetBranchAddress("muons.innerTrack.nValidStripHits", muons_innerTrack_nValidStripHits, &b_muons_innerTrack_nValidStripHits);
   fChain->SetBranchAddress("muons.innerTrack.nValidMuonHits", muons_innerTrack_nValidMuonHits, &b_muons_innerTrack_nValidMuonHits);
   fChain->SetBranchAddress("muons.innerTrack.nLostMuonHits", muons_innerTrack_nLostMuonHits, &b_muons_innerTrack_nLostMuonHits);
   fChain->SetBranchAddress("muons.innerTrack.nBadMuonHits", muons_innerTrack_nBadMuonHits, &b_muons_innerTrack_nBadMuonHits);
   fChain->SetBranchAddress("muons.innerTrack.nValidHits", muons_innerTrack_nValidHits, &b_muons_innerTrack_nValidHits);
   fChain->SetBranchAddress("muons.innerTrack.nLostHits", muons_innerTrack_nLostHits, &b_muons_innerTrack_nLostHits);
   fChain->SetBranchAddress("muons.innerTrack.quality", muons_innerTrack_quality, &b_muons_innerTrack_quality);
   fChain->SetBranchAddress("muons.innerTrack.sumPtIso03", muons_innerTrack_sumPtIso03, &b_muons_innerTrack_sumPtIso03);
   fChain->SetBranchAddress("muons.innerTrack.trackIso03", muons_innerTrack_trackIso03, &b_muons_innerTrack_trackIso03);
   fChain->SetBranchAddress("muons.outerTrack.p4.fCoordinates.fPt", muons_outerTrack_p4_fCoordinates_fPt, &b_muons_outerTrack_p4_fCoordinates_fPt);
   fChain->SetBranchAddress("muons.outerTrack.p4.fCoordinates.fEta", muons_outerTrack_p4_fCoordinates_fEta, &b_muons_outerTrack_p4_fCoordinates_fEta);
   fChain->SetBranchAddress("muons.outerTrack.p4.fCoordinates.fPhi", muons_outerTrack_p4_fCoordinates_fPhi, &b_muons_outerTrack_p4_fCoordinates_fPhi);
   fChain->SetBranchAddress("muons.outerTrack.p4.fCoordinates.fM", muons_outerTrack_p4_fCoordinates_fM, &b_muons_outerTrack_p4_fCoordinates_fM);
   fChain->SetBranchAddress("muons.outerTrack.ref.fCoordinates.fX", muons_outerTrack_ref_fCoordinates_fX, &b_muons_outerTrack_ref_fCoordinates_fX);
   fChain->SetBranchAddress("muons.outerTrack.ref.fCoordinates.fY", muons_outerTrack_ref_fCoordinates_fY, &b_muons_outerTrack_ref_fCoordinates_fY);
   fChain->SetBranchAddress("muons.outerTrack.ref.fCoordinates.fZ", muons_outerTrack_ref_fCoordinates_fZ, &b_muons_outerTrack_ref_fCoordinates_fZ);
   fChain->SetBranchAddress("muons.outerTrack.charge", muons_outerTrack_charge, &b_muons_outerTrack_charge);
   fChain->SetBranchAddress("muons.outerTrack.chi2", muons_outerTrack_chi2, &b_muons_outerTrack_chi2);
   fChain->SetBranchAddress("muons.outerTrack.nDOF", muons_outerTrack_nDOF, &b_muons_outerTrack_nDOF);
   fChain->SetBranchAddress("muons.outerTrack.errPt", muons_outerTrack_errPt, &b_muons_outerTrack_errPt);
   fChain->SetBranchAddress("muons.outerTrack.errEta", muons_outerTrack_errEta, &b_muons_outerTrack_errEta);
   fChain->SetBranchAddress("muons.outerTrack.errPhi", muons_outerTrack_errPhi, &b_muons_outerTrack_errPhi);
   fChain->SetBranchAddress("muons.outerTrack.errDxy", muons_outerTrack_errDxy, &b_muons_outerTrack_errDxy);
   fChain->SetBranchAddress("muons.outerTrack.errDz", muons_outerTrack_errDz, &b_muons_outerTrack_errDz);
   fChain->SetBranchAddress("muons.outerTrack.nPixelLayers", muons_outerTrack_nPixelLayers, &b_muons_outerTrack_nPixelLayers);
   fChain->SetBranchAddress("muons.outerTrack.nStripLayers", muons_outerTrack_nStripLayers, &b_muons_outerTrack_nStripLayers);
   fChain->SetBranchAddress("muons.outerTrack.nValidPixelHits", muons_outerTrack_nValidPixelHits, &b_muons_outerTrack_nValidPixelHits);
   fChain->SetBranchAddress("muons.outerTrack.nValidStripHits", muons_outerTrack_nValidStripHits, &b_muons_outerTrack_nValidStripHits);
   fChain->SetBranchAddress("muons.outerTrack.nValidMuonHits", muons_outerTrack_nValidMuonHits, &b_muons_outerTrack_nValidMuonHits);
   fChain->SetBranchAddress("muons.outerTrack.nLostMuonHits", muons_outerTrack_nLostMuonHits, &b_muons_outerTrack_nLostMuonHits);
   fChain->SetBranchAddress("muons.outerTrack.nBadMuonHits", muons_outerTrack_nBadMuonHits, &b_muons_outerTrack_nBadMuonHits);
   fChain->SetBranchAddress("muons.outerTrack.nValidHits", muons_outerTrack_nValidHits, &b_muons_outerTrack_nValidHits);
   fChain->SetBranchAddress("muons.outerTrack.nLostHits", muons_outerTrack_nLostHits, &b_muons_outerTrack_nLostHits);
   fChain->SetBranchAddress("muons.outerTrack.quality", muons_outerTrack_quality, &b_muons_outerTrack_quality);
   fChain->SetBranchAddress("muons.outerTrack.sumPtIso03", muons_outerTrack_sumPtIso03, &b_muons_outerTrack_sumPtIso03);
   fChain->SetBranchAddress("muons.outerTrack.trackIso03", muons_outerTrack_trackIso03, &b_muons_outerTrack_trackIso03);
   fChain->SetBranchAddress("muons.type", muons_type, &b_muons_type);
   fChain->SetBranchAddress("muons.sumPtIso03", muons_sumPtIso03, &b_muons_sumPtIso03);
   fChain->SetBranchAddress("muons.hcalIso03", muons_hcalIso03, &b_muons_hcalIso03);
   fChain->SetBranchAddress("muons.ecalIso03", muons_ecalIso03, &b_muons_ecalIso03);
   fChain->SetBranchAddress("muons.trackIso03", muons_trackIso03, &b_muons_trackIso03);
   fChain->SetBranchAddress("muons.pfIso04", muons_pfIso04, &b_muons_pfIso04);
   fChain->SetBranchAddress("muons.sumPtIso05", muons_sumPtIso05, &b_muons_sumPtIso05);
   fChain->SetBranchAddress("muons.hcalIso05", muons_hcalIso05, &b_muons_hcalIso05);
   fChain->SetBranchAddress("muons.ecalIso05", muons_ecalIso05, &b_muons_ecalIso05);
   fChain->SetBranchAddress("muons.trackIso05", muons_trackIso05, &b_muons_trackIso05);
   fChain->SetBranchAddress("muons.isGoodMuon", muons_isGoodMuon, &b_muons_isGoodMuon);
   fChain->SetBranchAddress("muons.caloComp", muons_caloComp, &b_muons_caloComp);
   fChain->SetBranchAddress("muons.segComp", muons_segComp, &b_muons_segComp);
   fChain->SetBranchAddress("muons.numberOfChambers", muons_numberOfChambers, &b_muons_numberOfChambers);
   fChain->SetBranchAddress("muons.numberOfMatches", muons_numberOfMatches, &b_muons_numberOfMatches);
   fChain->SetBranchAddress("muons.hltMatch", muons_hltMatch, &b_muons_hltMatch);
   fChain->SetBranchAddress("muons.eta_propagated", muons_eta_propagated, &b_muons_eta_propagated);
   fChain->SetBranchAddress("muons.phi_propagated", muons_phi_propagated, &b_muons_phi_propagated);
   fChain->SetBranchAddress("nTracks", &nTracks, &b_generalTracksSummary_nTracks);
   fChain->SetBranchAddress("nTracksHQ", &nTracksHQ, &b_generalTracksSummary_nTracksHQ);
   fChain->SetBranchAddress("p4.fCoordinates.fPt", &p4_fCoordinates_fPt, &b_PFMET_p4_fCoordinates_fPt);
   fChain->SetBranchAddress("p4.fCoordinates.fEta", &p4_fCoordinates_fEta, &b_PFMET_p4_fCoordinates_fEta);
   fChain->SetBranchAddress("p4.fCoordinates.fPhi", &p4_fCoordinates_fPhi, &b_PFMET_p4_fCoordinates_fPhi);
   fChain->SetBranchAddress("p4.fCoordinates.fM", &p4_fCoordinates_fM, &b_PFMET_p4_fCoordinates_fM);
   fChain->SetBranchAddress("sumEt", &sumEt, &b_PFMET_sumEt);
   fChain->SetBranchAddress("chargedEMEtFraction", &chargedEMEtFraction, &b_PFMET_chargedEMEtFraction);
   fChain->SetBranchAddress("chargedHadEtFraction", &chargedHadEtFraction, &b_PFMET_chargedHadEtFraction);
   fChain->SetBranchAddress("neutralEMEtFraction", &neutralEMEtFraction, &b_PFMET_neutralEMEtFraction);
   fChain->SetBranchAddress("neutralHadEtFraction", &neutralHadEtFraction, &b_PFMET_neutralHadEtFraction);
   fChain->SetBranchAddress("muonEtFraction", &muonEtFraction, &b_PFMET_muonEtFraction);
   fChain->SetBranchAddress("type6EtFraction", &type6EtFraction, &b_PFMET_type6EtFraction);
   fChain->SetBranchAddress("type7EtFraction", &type7EtFraction, &b_PFMET_type7EtFraction);
   fChain->SetBranchAddress("pv.position.fCoordinates.fX", &pv_position_fCoordinates_fX, &b_offlinePrimaryVerticesSummary_pv_position_fCoordinates_fX);
   fChain->SetBranchAddress("pv.position.fCoordinates.fY", &pv_position_fCoordinates_fY, &b_offlinePrimaryVerticesSummary_pv_position_fCoordinates_fY);
   fChain->SetBranchAddress("pv.position.fCoordinates.fZ", &pv_position_fCoordinates_fZ, &b_offlinePrimaryVerticesSummary_pv_position_fCoordinates_fZ);
   fChain->SetBranchAddress("pv.fake", &pv_fake, &b_offlinePrimaryVerticesSummary_pv_fake);
   fChain->SetBranchAddress("pv.nTracks", &pv_nTracks, &b_offlinePrimaryVerticesSummary_pv_nTracks);
   fChain->SetBranchAddress("pv.chi2", &pv_chi2, &b_offlinePrimaryVerticesSummary_pv_chi2);
   fChain->SetBranchAddress("pv.nDOF", &pv_nDOF, &b_offlinePrimaryVerticesSummary_pv_nDOF);
   fChain->SetBranchAddress("pv.covariance.fRep.fArray[6]", pv_covariance_fRep_fArray, &b_offlinePrimaryVerticesSummary_pv_covariance_fRep_fArray);
   fChain->SetBranchAddress("nVertices", &nVertices, &b_offlinePrimaryVerticesSummary_nVertices);
//    fChain->SetBranchAddress("pv.position.fCoordinates.fX", &pv_position_fCoordinates_fX, &b_offlinePrimaryVerticesWithBSSummary_pv_position_fCoordinates_fX);
//    fChain->SetBranchAddress("pv.position.fCoordinates.fY", &pv_position_fCoordinates_fY, &b_offlinePrimaryVerticesWithBSSummary_pv_position_fCoordinates_fY);
//    fChain->SetBranchAddress("pv.position.fCoordinates.fZ", &pv_position_fCoordinates_fZ, &b_offlinePrimaryVerticesWithBSSummary_pv_position_fCoordinates_fZ);
//    fChain->SetBranchAddress("pv.fake", &pv_fake, &b_offlinePrimaryVerticesWithBSSummary_pv_fake);
//    fChain->SetBranchAddress("pv.nTracks", &pv_nTracks, &b_offlinePrimaryVerticesWithBSSummary_pv_nTracks);
//    fChain->SetBranchAddress("pv.chi2", &pv_chi2, &b_offlinePrimaryVerticesWithBSSummary_pv_chi2);
//    fChain->SetBranchAddress("pv.nDOF", &pv_nDOF, &b_offlinePrimaryVerticesWithBSSummary_pv_nDOF);
//    fChain->SetBranchAddress("pv.covariance.fRep.fArray[6]", pv_covariance_fRep_fArray, &b_offlinePrimaryVerticesWithBSSummary_pv_covariance_fRep_fArray);
//    fChain->SetBranchAddress("nVertices", &nVertices, &b_offlinePrimaryVerticesWithBSSummary_nVertices);
   fChain->SetBranchAddress("position.fCoordinates.fX", &position_fCoordinates_fX, &b_offlineBeamSpot_position_fCoordinates_fX);
   fChain->SetBranchAddress("position.fCoordinates.fY", &position_fCoordinates_fY, &b_offlineBeamSpot_position_fCoordinates_fY);
   fChain->SetBranchAddress("position.fCoordinates.fZ", &position_fCoordinates_fZ, &b_offlineBeamSpot_position_fCoordinates_fZ);
   fChain->SetBranchAddress("type", &type, &b_offlineBeamSpot_type);
   fChain->SetBranchAddress("betaStar", &betaStar, &b_offlineBeamSpot_betaStar);
   fChain->SetBranchAddress("beamWidthX", &beamWidthX, &b_offlineBeamSpot_beamWidthX);
   fChain->SetBranchAddress("beamWidthY", &beamWidthY, &b_offlineBeamSpot_beamWidthY);
   fChain->SetBranchAddress("emittanceX", &emittanceX, &b_offlineBeamSpot_emittanceX);
   fChain->SetBranchAddress("emittanceY", &emittanceY, &b_offlineBeamSpot_emittanceY);
   fChain->SetBranchAddress("dxdz", &dxdz, &b_offlineBeamSpot_dxdz);
   fChain->SetBranchAddress("dydz", &dydz, &b_offlineBeamSpot_dydz);
   fChain->SetBranchAddress("sigmaZ", &sigmaZ, &b_offlineBeamSpot_sigmaZ);
   fChain->SetBranchAddress("covariance.fRep.fArray[28]", covariance_fRep_fArray, &b_offlineBeamSpot_covariance_fRep_fArray);
   fChain->SetBranchAddress("median", &median, &b_KT6Area_median);
   fChain->SetBranchAddress("sigma", &sigma, &b_KT6Area_sigma);
}

Bool_t MuBenchSelector::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

#endif // #ifdef MuBenchSelector_cxx
