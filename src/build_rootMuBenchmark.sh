#!/bin/bash

BIN_NAME=${1:-"rootbenchmark"}
TMP_NAME="benchmark.tmp"

echo "Compiling to ${TMP_NAME}..."
g++ root.MuBenchmark.cpp MuBenchSelector.C -o benchmark.tmp $(root-config --cflags --libs) || exit
echo " > Moving to ${BIN_NAME}"
[[ -f "${BIN_NAME}" ]] && rm -f "${BIN_NAME}"
mv ${TMP_NAME} ${BIN_NAME}

