// core includes
# include <string>
# include <vector>
# include <iostream>
# include <fstream>
# include <stdio.h>
# include <ctime>
// root includes
# include <TError.h>
# include <TChain.h>
# include "MuBenchSelector.h"

int globalVerbosity = 0;

int main(int argcount, char** argarray) {
	gErrorIgnoreLevel = kError; // turn off ROOT warnings
	std::string inputFileFile = "";
	std::vector<std::string> inputFileList;
	for ( int index = 1; index < argcount; ++index )
	{
		if ( strcmp("-h", argarray[index]) == 0 || strcmp("--help", argarray[index]) == 0 ) {
			std::cerr << "usage: " << argarray[0] << "[--help] [--input-catalogue CAT] [--input-file FILE [FILE ...]]" << std::endl;
			std::cerr << std::endl;
			std::cerr << "Perform a ROOT benchmark, reading Kappa Muon files" << std::endl;
			std::cerr << std::endl;
			std::cerr << "optional arguments:" << std::endl;
			std::cerr << "  -h, --help" << std::endl;
			std::cerr << "                   show this help message and exit" << std::endl;
			std::cerr << "  -f, --input-file FILE [FILE ...]" << std::endl;
			std::cerr << "                   add the ROOT file FILE for processing" << std::endl;
			std::cerr << "  -c, --input-catalogue CAT" << std::endl;
			std::cerr << "                   add files listed in CAT for processing" << std::endl;
			return 0;
		}
		if ( strcmp("-c", argarray[index]) == 0 || strcmp("--input-catalogue", argarray[index]) == 0 ) {
			index++;
			inputFileFile = std::string(argarray[index]);
			continue;
		}
		if ( strcmp("-f", argarray[index]) == 0 || strcmp("--input-file", argarray[index]) == 0 ) {
			index++;
			while (index < argcount ) {
				if ( argarray[index][0] != '-' ) {
					inputFileList.push_back(std::string(argarray[index++]));
				}
			}
			continue;
		}
		if ( strcmp("-v", argarray[index]) == 0 || strcmp("--verbose", argarray[index]) == 0 ) {
			++globalVerbosity;
			continue;
		}
		std::cerr << "Unknown CLI argument: '" << argarray[index] << "'" << std::endl;
		return 1;
	}
	if ( inputFileFile.empty() && inputFileList.empty() )
	{
		std::cerr << "No input catalogue/files specified." << std::endl;
		return 2;
	}

	if ( globalVerbosity >= 1  )
		std::cerr << "== Initialization ==" << std::endl;
	int   utimestampInit    = time(0);

	if ( globalVerbosity >= 1  )
		std::cerr << "Creating TChain..." << std::endl;
	TChain *events = new TChain("Events", "MuChain");
	for (int index = 0; index < inputFileList.size(); ++index)
	{
		events->Add(inputFileList[index].c_str());
	}
	if ( ! inputFileFile.empty() )
	{
		std::ifstream inputList;
		inputList.open(inputFileFile.c_str());
		std::string lineBuffer;
		while ( std::getline(inputList, lineBuffer) )
		{
			if ( ! lineBuffer.empty() )
				events->Add(lineBuffer.c_str());
		}
		inputList.close();
	}
	if ( globalVerbosity >= 1  )
		std::cerr << "<Added " << events->GetListOfFiles()->GetLast() + 1 << " files>" << std::endl;
	
	if ( globalVerbosity >= 1  )
		std::cerr << "[INIT] \u0394t " << time(0) - utimestampInit << "s" << std::endl;

	if ( globalVerbosity >= 1  )
		std::cerr << "== Processing ==" << std::endl;
	MuBenchSelector* selector = new MuBenchSelector();

	if ( globalVerbosity >= 1  )
		std::cerr << "Iterating events..." << std::endl;
	events->Process(selector);
	if ( globalVerbosity >= 1  )
		std::cerr << "<done>" << std::endl;
	return 0;
}
