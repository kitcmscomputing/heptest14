#!/usr/bin/python

import plot
import rtmlib.logImport as LI
import rtmlib.plotUtils as PU

print "Initializing..."
baseTitle=r"ROOT best case"
title=plot.plotTitle(r"ROOT best case")
plotScale = (4*1.5,3*1.5)
# mb/s/core
mbpspclabels = plot.plotAxisLabels(
	xlabel = "Used Cores",
	ylabel = "Throughput",
	yunit = "MB/s/Core"
	)
mbpspcRanges77 = plot.plotAxesRange(
	xrange = [0.5, 8.5],
	yrange = [0.0, 40.0]
	)
mbpspcRangesT3 = plot.plotAxesRange(
	xrange = [0.5, 24.5],
	yrange = [0.0, 40.0]
	)
# markers
goalline = plot.plotAxisLine(
	coord      = 20,
	linestyle  = '--'
	)
oneGBS = plot.functionDrawable(
		drawFunc = lambda x : 1 * 128.0 / x,
		xRange = [1, 25],
		color = 'red',
		)
twoGBS = plot.functionDrawable(
		drawFunc = lambda x : 2 * 128.0 / x,
		xRange = [1, 25],
		color = 'blue',
		)
# cpu pct
cpupctlabels = plot.plotAxisLabels(
	xlabel = "CPU",
	ylabel = r"CPU Usage",
	yunit = r"\%"
	)
cpupctRanges = plot.plotAxesRange(
	xrange = [0.5, 24.5],
	yrange = [0.0, 100.0]
	)
# EKPLX77
print "EKPLX77"
print " o Importing..."
logData = LI.importLogFiles(
	[
		"/local/scratch/hdd/mfischer/heptest/logs/log.ID-EKP77ssd.NCpu-8*.out",
		"/local/scratch/hdd/mfischer/heptest/logs/log.ID-EKP77hdd.NCpu-8*.out",
	],
	matchKey = lambda content : "%s.%s.P%d"%(content["nickname"], content["ID"], content.get("samplepass",0))
	)
print " o Plotting..."
plot.makePlot(
	'ekplx77.ssd.mbpspc.png',
	drawables = [
	plot.dataDrawable(
		logData,
		item = 'mbpspc',
		keyList = [ "R1x%d.EKP77ssd.P1" % threads for threads in range(1,9) ],
		name = "EKP77: SSD"
		),
	],
	size = plotScale,
	plotFeatures=[
		plot.plotTitle(baseTitle + r" - EKP Desktop"),
		mbpspclabels,
		mbpspcRanges77,
		goalline
		],
	)
PU.resetGetter()
plot.makePlot(
        'ekplx77.hdd.mbpspc.png',
        drawables = [
        plot.dataDrawable(
                logData,
                item = 'mbpspc',
                keyList = [ "R1x%d.EKP77hdd.P1" % threads for threads in range(1,9) ],
                name = "EKP77: HDD"
                ),
        ],
        size = plotScale,
        plotFeatures=[
			plot.plotTitle(baseTitle + r" - EKP Desktop"),
                mbpspclabels,
                mbpspcRanges77,
                goalline
                ],
        )
PU.resetGetter()
# EKPVM77
print "EKPVM77"
print " o Importing..."
logData = LI.importLogFiles(
	[
		"/local/scratch/hdd/mfischer/heptest/logs/log.ID-ECM77ssd.NCpu-6*.out",
	],
	matchKey = lambda content : "%s.%s.P%d"%(content["nickname"], content["ID"], content.get("samplepass",0))
	)
print " o Plotting..."
plot.makePlot(
	'ekpvm77.ssd.mbpspc.png',
	drawables = [
	plot.dataDrawable(
		logData,
		item = 'mbpspc',
		keyList = [ "R1x%d.EVM77ssd.P1" % threads for threads in range(1,7) ],
		name = "EVM77: SSD"
		),
	],
	size = plotScale,
	plotFeatures=[
		plot.plotTitle(baseTitle + r" - EKP Desktop VM"),
		mbpspclabels,
		mbpspcRanges77,
		goalline
		],
	)
PU.resetGetter()
# EKPT3
print "EKPT3"
print " o Importing..."
logData = LI.importLogFiles(
	[
		"/local/scratch/hdd/mfischer/heptest/logs/job_*_log.ID-EKPT3Lcl.NCpu-24.JN-*.out",
		"/local/scratch/hdd/mfischer/heptest/logs/job_*_log.ID-EKPT3Strg.NCpu-24.JN-*.out",
	],
	matchKey = lambda content : "%s.%s.P%d"%(content["nickname"], content["ID"], content.get("samplepass",0))
	)
print " o Plotting..."
plot.makePlot(
	'ekptier3.nfs.mbpspc.png',
	drawables = [
	plot.dataDrawable(
		logData,
		item = 'mbpspc',
		keyList = [ "R1x%d.EKPT3Strg.P1" % threads for threads in range(1,25) ],
		name = "blus00X: FS5"
		),
	],
	size = plotScale,
	plotFeatures=[
		plot.plotTitle(baseTitle + r" - EKP Cluster"),
		mbpspclabels,
		mbpspcRangesT3,
		goalline
		],
	)
PU.resetGetter()
plot.makePlot(
	'ekptier3.nfs.gbs.mbpspc.png',
	drawables = [
	plot.dataDrawable(
		logData,
		item = 'mbpspc',
		keyList = [ "R1x%d.EKPT3Strg.P1" % threads for threads in range(1,25) ],
		name = "blus00X: FS5"
		),
	]
	+
	[
	plot.functionDrawable(
		drawFunc = lambda x : 1 * 120.0 / x,
		xRange = [1, 24],
		color = 'red',
		)
	],
	size = plotScale,
	plotFeatures=[
		plot.plotTitle(baseTitle + r" - EKP Cluster"),
		mbpspclabels,
		mbpspcRangesT3,
		goalline
		],
	)
PU.resetGetter()
plot.makePlot(
	'ekptier3.hdd.mbpspc.png',
	drawables = 
	[
		plot.dataDrawable(
			logData,
			item = 'mbpspc',
			keyList = [ "R1x%d.EKPT3Lcl.P1" % threads for threads in range(1,25) ],
			name = "EKPT3: RAID"
			),
	]
	+
	[
	oneGBS,
	twoGBS,
	],
	size = plotScale,
	plotFeatures=[
		title,
		mbpspclabels,
		mbpspcRangesT3,
		goalline
		],
	)
PU.resetGetter()
# NAF2.0
print "NAF2"
print " o Importing..."
hostlist = (
"bird036",
"bird037",
"bird038",
#"bird158",
"bird166",
"bird173",
#"bird178",
"bird179",
"bird187",
"bird189",
#"bird191",
"bird192",
#"bird198",
"bird201",
#"bird203",
#"bird204",
)
hostGroups = {
	0 : ["bird036","bird037","bird038",],
	1 : ["bird158","bird166","bird173","bird178","bird179","bird187","bird189","bird191","bird192","bird198",],
	2 : ["bird201","bird203","bird204","bird223",],
#	3 : ["bird-cfel01",]
}
def getHostGroup(hostname):
	for groupKey in hostGroups:
		if hostname in hostGroups[groupKey]:
			return groupKey

logData = LI.importLogFiles(
	[
		"/local/scratch/hdd/mfischer/heptest/logs/job_*_log.ID-NAFSns*.NCpu-24.JN-*.out",
	],
	matchKey = lambda content : "%s.%s.%s.P%d"%(content["nickname"], content["ID"], getHostGroup(content["hostname"]), content.get("samplepass",0))
	)
print " o Plotting..."
plot.makePlot(
	'nafbird.sns.mbpspc.png',
	drawables =
	[
		plot.dataDrawable(
			logData,
			item = 'mbpspc',
			keyList = [ "R1x%d.NAFSns24.%s.P1" % ( threads, groupKey) for threads in range(1,25) ],
				name = "bird%sXX : SONAS" % groupKey
				)
		for groupKey in hostGroups
	],
	size = plotScale,
	plotFeatures=[
		plot.plotTitle(baseTitle + r" - NAF 2.0"),
		mbpspclabels,
		mbpspcRangesT3,
		goalline,
		],
	)
PU.resetGetter()
plot.makePlot(
	'nafbird.sns.gbs.mbpspc.png',
	drawables =
	[
		plot.dataDrawable(
			logData,
			item = 'mbpspc',
			keyList = [ "R1x%d.NAFSns24.%s.P1" % ( threads, groupKey) for threads in range(1,25) ],
				name = "bird%sXX : SONAS" % groupKey
				)
		for groupKey in hostGroups
	]
	+
	[
	oneGBS,
	twoGBS
	],
	size = plotScale,
	plotFeatures=[
		plot.plotTitle(baseTitle + r" - NAF 2.0"),
		mbpspclabels,
		mbpspcRangesT3,
		goalline,
		],
	)
PU.resetGetter()

plot.makePlot(
	'nafbird.1xx.gbs.mbpspc.png',
	drawables =
	[
		plot.dataDrawable(
			logData,
			item = 'mbpspc',
			keyList = [ "R1x%d.NAFSns24.%s.P1" % ( threads, groupKey) for threads in range(1,25) ],
				name = "bird%sXX : SONAS" % groupKey
				)
		for groupKey in [1]
	]
	+
	[
	oneGBS
	],
	size = plotScale,
	plotFeatures=[
		plot.plotTitle(baseTitle + r" - NAF 1xx Architecture"),
		mbpspclabels,
		mbpspcRangesT3,
		goalline,
		],
	)
PU.resetGetter()
