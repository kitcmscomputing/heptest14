#!/usr/bin/python

import plot
import rtmlib.logImport as LI
import rtmlib.plotUtils as PU


logData = LI.importLogFiles(
	[
		"/local/scratch/hdd/mfischer/heptest/logs/job_*_log.ID-EKPT3Lcl.NCpu-24.JN-*.out",
		"/local/scratch/hdd/mfischer/heptest/logs/job_*_log.ID-EKPT3Strg.NCpu-24.JN-*.out",
		"/local/scratch/hdd/mfischer/heptest/logs/log.ID-EKP77ssd.NCpu-8*.out",
		"/local/scratch/hdd/mfischer/heptest/logs/log.ID-EKP77hdd.NCpu-8*.out",
		"/local/scratch/hdd/mfischer/heptest/logs/log.ID-ECM77ssd.NCpu-6*.out",
		"/local/scratch/hdd/mfischer/heptest/logs/log.ekpcms6*.out",
	],
	matchKey = lambda content : "%s.%s.P%d"%(content["nickname"], content["ID"], content.get("samplepass",0))
	)

#mb/s/core
mbpspcDrawables = [
	plot.dataDrawable(
		logData,
		item = 'mbpspc',
		keyList = [ "R1x%d.EKPT3Lcl.P1" % threads for threads in range(1,25) ],
		name = "EKPT3: HDD"
	),
	plot.dataDrawable(
		logData,
		item = 'mbpspc',
		keyList = [ "R1x%d.EKPT3Strg.P1" % threads for threads in range(1,25) ],
		name = "EKPT3: Stg"
	),
	plot.dataDrawable(
		logData,
		item = 'mbpspc',
		keyList = [ "R1x%d.EKP77ssd.P1" % threads for threads in range(1,9) ],
		name = "EKP77: SSD"
	),
	plot.dataDrawable(
		logData,
		item = 'mbpspc',
		keyList = [ "R1x%d.EKP77hdd.P1" % threads for threads in range(1,9) ],
		name = "EKP77: HDD"
	),
	plot.dataDrawable(
		logData,
		item = 'mbpspc',
		keyList = [ "R1x%d.EVM77ssd.P1" % threads for threads in range(1,7) ],
		name = "EVM77: SSD"
	),
]
cpupctThreshold = [
	plot.dataThresholdDrawable(
		logData,
		item = 'cpupct',
		keyList = [ "R1x%d.EKPCMS6.P1" % threads for threads in range(1,25) ],
		name = "EKPT3: HDD 1",
		threshold = 90,
		linestyle = '--'
	),
	plot.dataThresholdDrawable(
		logData,
		item = 'cpupct',
		keyList = [ "R1x%d.EKPCMS6Strg.P1" % threads for threads in range(1,25) ],
		name = "EKPT3: Stg 1",
		threshold = 90,
		linestyle = ':'
	),
	plot.dataThresholdDrawable(
		logData,
		item = 'cpupct',
		keyList = [ "R1x%d.EKP77ssd.P1" % threads for threads in range(1,9) ],
		name = "EKP77: SSD 1",
		threshold = 90,
		linestyle = '--'
	),
	plot.dataThresholdDrawable(
		logData,
		item = 'cpupct',
		keyList = [ "R1x%d.EKP77hdd.P1" % threads for threads in range(1,9) ],
		name = "EKP77: HDD 1",
		threshold = 90,
		linestyle = '--'
	),
	plot.dataThresholdDrawable(
		logData,
		item = 'cpupct',
		keyList = [ "R1x%d.EVM77ssd.P1" % threads for threads in range(1,7) ],
		name = "EVM77: SSD",
		threshold = 90,
		linestyle = '--'
	),
]
for index in range(len(mbpspcDrawables)):
	cpupctThreshold[index].setColor(mbpspcDrawables[index].getColor())

cpupctDrawables = [
	plot.dataDrawable(
		logData,
		item = 'cpupct',
		keyList = [ "R1x%d.EKPT3Lcl.P1" % threads for threads in range(1,25) ],
		name = "EKPT3: HDD 1"
	),
	plot.dataDrawable(
		logData,
		item = 'cpupct',
		keyList = [ "R1x%d.EKPT3Lcl.P2" % threads for threads in range(1,25) ],
		name = "EKPT3: HDD 2"
	),
	plot.dataDrawable(
		logData,
		item = 'cpupct',
		keyList = [ "R1x%d.EKPT3Strg.P1" % threads for threads in range(1,25) ],
		name = "EKPT3: Stg 1"
	),
	plot.dataDrawable(
		logData,
		item = 'cpupct',
		keyList = [ "R1x%d.EKPT3Strg.P2" % threads for threads in range(1,25) ],
		name = "EKPT3: Stg 2"
	),
	plot.dataDrawable(
		logData,
		item = 'cpupct',
		keyList = [ "R1x%d.EKP77ssd.P1" % threads for threads in range(1,9) ],
		name = "EKP77: Stg 1"
	),
	plot.dataDrawable(
		logData,
		item = 'cpupct',
		keyList = [ "R1x%d.EKP77ssd.P2" % threads for threads in range(1,9) ],
		name = "EKP77: Stg 2"
	),
]

title=plot.plotTitle(r"ROOT best case")
# mb/s/core
mbpspclabels = plot.plotAxisLabels(
	xlabel = "Instances",
	ylabel = "Throughput",
	yunit = "MB/s/Core"
	)
mbpspcRanges = plot.plotAxesRange(
	xrange = [0.5, 24.5],
	yrange = [0.0, 40.0]
	)
# markers
goalline = plot.plotAxisLine(
	coord      = 20,
	linestyle  = '--'
	)
# cpu pct
cpupctlabels = plot.plotAxisLabels(
	xlabel = "CPU",
	ylabel = r"CPU Usage",
	yunit = r"\%"
	)
cpupctRanges = plot.plotAxesRange(
	xrange = [0.5, 24.5],
	yrange = [0.0, 100.0]
	)

plot.makePlot(
	'et3.mbpspc.png',
	drawables = mbpspcDrawables, # + cpupctThreshold,
	size = (4*1.5,3*1.5),
	plotFeatures=[
		title,
		mbpspclabels,
		mbpspcRanges,
		goalline
		],
	)
PU.resetGetter()
plot.makePlot(
	'et3.cpupct.png',
	drawables = cpupctDrawables,
	size = (4*1.5,3*1.5),
	plotFeatures=[
		title,
		cpupctlabels,
		cpupctRanges
		],
	)
