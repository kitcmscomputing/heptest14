#!/usr/bin/python

from __future__ import print_function

import glob
import json
from rtmlib.utility import getMeanStdDev
from utility import vprint, eprint, verbosityLevels

def dataProvider( logFileGlobList = [] ):
	vprint("Parsing log files", verbosityLevel = verbosityLevels.SYSTEM)

def importLogFiles( logFileGlobList, matchKey = lambda content : content["nickname"]+content["ID"]):
	vprint("Parsing log files:", verbosityLevel = verbosityLevels.SYSTEM)
	rawDataList=[]
	for logFileGlob in logFileGlobList:
		for logFile in glob.iglob(logFileGlob):
			try:
				rawDataList.extend(readLogJSON(logFile))
			except ValueError:
				try:
					print("Fallback import for file:", logFile)
					rawDataList.extend(readLogRaw(logFile))
				except:
					pass
	return consolidateRawData(rawDataList, matchKey=matchKey)

def readLogRaw(logFile):
	logData = []
	primaryKeys = (None, "fullname", "kbps", None, "kbpspc", None)
	secondaryKeys = ("datasize", "walltime", "usertime", "systime", "cpupct", "hostname", "ID")

	with open(logFile) as rawdata:
		for line in rawdata.readlines():
			if not line.startswith("()"):
				continue
			# extract raw data
			content = dict(zip(secondaryKeys,line.split("[")[1].split()))
			content.update(dict(zip(primaryKeys,line.split("[")[0].split())))
			for key in secondaryKeys:
				content[key] = content[key][2:]
			# derive and store
			logData.append(formatLogData(content))
	return logData

def readLogJSON(logFile, desiredType = None):
	with open(logFile) as fileObject:
		logContent = json.load(fileObject)
		if ( desiredType ) and ( desiredType not in logContent["type"] ):
			vprint("Skipping undesired logType %s versus desiredType(s) %s" % (logContent["type"], desiredType), verbosityLevel = verbosityLevels.SETTING)
			vprint("File: %s" % logFile, verbosityLevel = verbosityLevels.INTERNAL)
			return []
		logData=[]
		for dataKey in logContent["data"]:
			if dataKey.startswith("__"):
				continue
			try:
				logData.append(formatLogData(logContent["data"][dataKey]))
			except ValueError:
				pass
	return logData

def formatLogData(dataDict):
	"""
	Derive data from legacy log dicts containing encoded data
	"""
	# skip any that cannot be converted properly
	try:
		# clean data
		try:
			del dataDict[None]
		except KeyError:
			pass
		# extract data
		if "nickname" not in dataDict:
			dataDict["nickname"] = dataDict["fullname"].split("@")[1]
		if "threads" not in dataDict:
			dataDict["threads"] = dataDict["nickname"].split("x")[0][1:]
		if "instances" not in dataDict:
			dataDict["instances"] = dataDict["nickname"].split("x")[1]
		# strip data
		try:
			dataDict["cpupct"] = dataDict["cpupct"].rstrip("%")
		except KeyError:
			pass
		# convert numerical values
		for key in dataDict:
			try:
				dataDict[key] = float(dataDict[key])
			except:
				pass
		# calculated values
		if "cpu" not in dataDict:
			dataDict["cpu"] = dataDict["cpupct"] / 100
		dataDict["mbps"] = dataDict["kbps"] / 1000
		dataDict["mbpspc"] = dataDict["kbpspc"] / 1000
		return dataDict
	except:
		raise ValueError
	

def consolidateRawData(rawDataList, matchKey = lambda content : content["nickname"]+content["ID"]):
	# order individual data by their keys to chunks
	orderedData = {}
	for rawData in rawDataList:
		key = matchKey(rawData)
		try:
			for item in rawData:
				orderedData[key][item].append(rawData[item])
		except KeyError:
			orderedData[key]={}
			for item in rawData:
				orderedData[key][item] = [rawData[item]]
	# compress numeric data to (mean, deviation) and other to unique per chunk
	consolidatedData = {}
	for key in orderedData:
		consolidatedData[key] = {}
		for item in orderedData[key]:
			try:
				consolidatedData[key][item] = getMeanStdDev(orderedData[key][item])
			except TypeError:
				if len(set(orderedData[key][item])) == 1:
					consolidatedData[key][item] = orderedData[key][item][0]
	return consolidatedData
