#!/usr/bin/python

from __future__ import print_function

import math

#######################
#######################
#### Classes
#######################
#######################

class Enum(object):
	def __init__(self, *enumList, **setEnumList):
		self.__dict__ = dict( zip( enumList, [ 2**ind for ind in range(len(enumList)) ] ) , **setEnumList)
	def __setitem__(self, name, value):
		self.__dict__[name] = value
	def add(self, name):
		self.__dict__[name] = 2**(math.log(max(self.__dict__.values(),2)) + 1)

#######################
#######################
#### Verbosity
#######################
#######################

# ALWAYS:   Must be always printed
# SYSTEM:   What the program is doing
# SETTING:  Actions caused by user Settings
# INTERNAL: Additional process information for previous output
# DETAIL:   Very finegrained information

verbosityLevels = Enum("ALWAYS", "SYSTEM", "SETTING", "INTERNAL", "DETAIL")
verbosityPrefix = {
	verbosityLevels.ALWAYS :	"     ",
	verbosityLevels.SYSTEM :	"o    ",
	verbosityLevels.SETTING :	"-o   ",
	verbosityLevels.INTERNAL :	"--o  ",
	verbosityLevels.DETAIL :	"---o ",
	}

def vprint(message, verbosityLevel = verbosityLevels.ALWAYS, minLevel = True):
	if outVerbosity() == verbosityLevel or ( minLevel and outVerbosity() > verbosityLevel ):
		print("[%s] %s %s" % (time.asctime(), verbosityPrefix[verbosityLevel], message))

def eprint(message, verbosityLevel = verbosityLevels.ALWAYS, minLevel = True):
	if errVerbosity() == verbosityLevel or ( minLevel and errVerbosity() > verbosityLevel ):
		print("<%s> %s %s" % (time.asctime(), verbosityPrefix[verbosityLevel], message), file = sys.stderr)


def globalSetupProxy(fun, default, new = None):
	if new != None:
		fun.setting = new
	try:
		return fun.setting
	except:
		return default

def outVerbosity(new = None):
	return globalSetupProxy(outVerbosity, verbosityLevels.ALWAYS, new)
def errVerbosity(new = None):
	return globalSetupProxy(errVerbosity, verbosityLevels.ALWAYS, new)

#######################
#######################
#### MATH
#######################
#######################
def getMeanStdDev(iterable):
	mean = 0
	mvar = 0
	mind = 0
	for value in iterable:
		# running mean/variance
		mind += 1
		sfactor = value - mean
		mean += ( value - mean ) / mind
		mvar += sfactor * ( value - mean)
	return mean, math.sqrt(mvar/max(mind-1,1))
