#!/usr/bin/bash

_PtPerInch = 72.0
idxColor = 0
idxMarker = 0

def PtToInch(pt):
	return pt / _PtPerInch
def InchToPt(inch):
	return inch * _PtPerInch

def getIntensityCMap(hmin = 0, hmax = 1, smin = 1, smax = 1, vmin = 1, vmax = 1, amin = 1, amax = 1):
	# shift to expected range
	while hmax - hmin > 1:
		hmax = hmax-1
	while hmax - hmin < -1:
		hmax = hmax+1
	while (hmin < 0 or hmax < 0):
		hmax, hmin = hmax+1, hmin+1
	while (hmin > 1 and hmax > 1):
		hmax, hmin = hmax-1, hmin-1

	# get additional sampling point for robustness
	midx = 0.5
	if hmax > 1:
		midx = (1 - hmin) / (hmax - hmin)
	elif hmin > 1:
		midx = (hmin - 1) / (hmin - hmax)
	midxr = 1 - midx

	r0,g0,b0 = colorsys.hsv_to_rgb( hmin, smin, vmin)
	r5,g5,b5 = colorsys.hsv_to_rgb( (hmin*midxr+hmax*midx), (smin*midxr+smax*midx), (vmin*midxr+vmax*midx))
	r1,g1,b1 = colorsys.hsv_to_rgb( hmax, smax, vmax)
	cmap = {
		"red": ((0.0, r0,r0),
				(midx, r5,r5),
				(1.0, r1,r1)),
		"green": ((0.0, g0,g0),
				(midx, g5,g5),
				(1.0, g1,g1)),
		"blue": ((0.0, b0,b0),
				(midx, b5,b5),
				(1.0, b1,b1)),
			}
	return cmap

def resetGetter():
	global idxColor
	global idxMarker
	idxColor = 0
	idxMarker = 0

def getColorName():
	global idxColor
	availColors = ["orange", "red", "blue", "green", "magenta", "cyan"]
	idxColor = (idxColor + 1) % len(availColors)
	return availColors[idxColor];

def getMarkerName():
	global idxMarker
	availMarkers = [ "<", "D", "s", "o", (6, 1, 0), (4, 1, 45) ]
	idxMarker = (idxMarker + 1) % len(availMarkers)
	return availMarkers[idxMarker];