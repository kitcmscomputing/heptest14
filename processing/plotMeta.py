#!/usr/bin/python

import matplotlib

from drawUtils import getIntensityCMap

argDefault = object()

class processedDataArgDrawable():
	__init__(self, data, arg, keyList, indexList = None, name, shortName = None, style = None)
		# defaults
		if not indexList:
			indexList = range(1, len(keyList)+1)
		if not shortName:
			shortName = name
		if not style:
			style = {}
		
		self.arg = arg
		self.keyList = keyList
		self.indexList = indexList
		self.name = name
		self.shortName = shortName
		self.style = style
	
	def getXList(self):
		return [ self.data[key][self.arg](0) for key in self.keyList ]
	def getXErrList(self):
		return [ self.data[key][self.arg](1) for key in self.keyList ]
	def getYList(self):
		return indexList
	
	def draw(self, figure, axis, defStyle = {}):
		self.drawError(figure, axis, defStyle)
	def drawError(self, figure, axis, defStyle = {}):
		axis.errorbar(
			self.getXList(),
			self.getYList(),
			xerr = self.getXErrList(),
			lw = self.style.get("linewidth", defStyle.get("linewidth",1))
			ls = self.style.get("linestyle", defStyle.get("linestyle",1))


def createPlot(outputName, drawables=[], defStyle={}, size=1, aspectRatio=1):