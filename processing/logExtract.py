#!/usr/bin/python

import hashlib
import glob
import math

def readLog(logFile):
	"""read the data from a log file
	
	> logfile	file containing the log from a root test
	< logDataList	list of dictionaries containing data"""
	logData=[]
	contentKeys=[None,"nickname","KBpS",None,"KBpSpC",None,None,"datasize","walltime","usertime","systime","cpu","location","ID",None]
	
	with open(logFile) as inFile:
		for line in inFile.readlines():
			if line.startswith("()"):
				# extract raw data
				content = dict(zip(contentKeys,line.split()))
				del content[None]
				# extract coded data
				content["threads"] = content["nickname"][1:]
				content["mode"] = content["nickname"][0]
				content["hostname"] = content["location"].split(":")[0][1:]
				content["directory"] = content["location"].split(":")[1].rstrip("/")
				content["basedirectory"] = content["location"].rsplit("/",1)[0]
				for key in ["datasize","walltime","usertime","systime","cpu"]:
					content[key] = content[key][1:]
				# convert formats
				for key in ["KBpS","KBpSpC","datasize","walltime","usertime","systime","threads"]:
					content[key] = float(content[key])
				content["cpu"] = float(content["cpu"][:-1])/100
				# store
				logData.append(content)
	return logData

def importLogs(logFileGlobList):
	"""read data from list of logs
	
	> logFileGlobList	list of file globs containing logs
	< logDataList		list of dictionaries containing data"""
	logDataList=[]
	for logFileGlob in logFileGlobList:
		for logFile in glob.iglob(logFileGlob):
			logDataList.extend(readLog(logFile))
	return logDataList

def collectData(rawDataList, keyFunction=lambda content: content["nickname"]+content["hostname"]):
	"""collect data from multiple runs into data points"""
	
	# sort list into dict
	dataDict = {}
	for rawData in rawDataList:
		key = keyFunction(rawData)
		if not key in dataDict:
			dataDict[key] = {}
			for ind in rawData:
				dataDict[key][ind] = []
		for ind in rawData:
			dataDict[key][ind].append(rawData[ind])
	# compress keys to (avg + dev)
	compressedDict = {}
	for key in dataDict:
		compressedDict[key] = {}
		for ind in dataDict[key]:
			try:
				compressedDict[key][ind] = getMeanStdDev(dataDict[key][ind])
			except TypeError:
				if len(set(dataDict[key][ind])) == 1:
					compressedDict[key][ind] = dataDict[key][ind][0]
	return compressedDict

def getMeanStdDev(iterable):
	mean = 0
	mvar = 0
	mind = 0
	for value in iterable:
		# running mean/variance
		mind += 1
		sfactor = value - mean
		mean += ( value - mean ) / mind
		mvar += sfactor * ( value - mean)
	return mean, math.sqrt(mvar/max(mind-1,1))