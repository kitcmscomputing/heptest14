#!/usr/bin/python

import plot
import rtmlib.logImport as LI
import rtmlib.plotUtils as PU
 
logData = LI.importLogFiles(
	[
		"/local/scratch/hdd/mfischer/heptest/out/log.RTMM.*.out",
		#"/local/scratch/hdd/mfischer/heptest/out/log.RTMM.SSD.out",
		#"/local/scratch/hdd/mfischer/heptest/out/log.RTMM.SSD.151113.out"
	]
	)

#mb/s/core
mbpspcDrawables = [
	plot.dataDrawable(
		logData,
		item = 'mbpspc',
		keyList = [ "R1x%dEKPhdd" % threads for threads in range(1,9) ],
		name = "Desktop (HDD): ROOT"
	),
	plot.dataDrawable(
		logData,
		item = 'mbpspc',
		keyList = [ "P2x%dEKPhdd" % threads for threads in range(1,5,1) ],
		indexList = range(2,9,2),
		name = "Desktop (HDD): Proof 2"
	),
	#plot.dataDrawable(
		#logData,
		#item = 'mbpspc',
		#keyList = [ "P8x1EKPhdd" ],
		#indexList = [ 8 ],
		#name = "Desktop (HDD): Proof 8"
	#),
	plot.dataDrawable(
		logData,
		item = 'mbpspc',
		keyList = [ "R1x%dEKPssd" % threads for threads in range(1,9) ],
		name = "Desktop (SSD): ROOT"
	),
	plot.dataDrawable(
		logData,
		item = 'mbpspc',
		keyList = [ "P2x%dEKPssd" % threads for threads in range(1,5,1) ],
		indexList = range(2,9,2),
		name = "Desktop (SSD): Proof 2"
	),
	#plot.dataDrawable(
		#logData,
		#item = 'mbpspc',
		#keyList = [ "P8x1EKPssd" ],
		#indexList = [ 8 ],
		#name = "Desktop (SSD): Proof 8"
	#)
]
cpupctDrawables = [
	plot.dataDrawable(
		logData,
		item = 'cpupct',
		keyList = [ "R1x%dEKPhdd" % threads for threads in range(1,9) ],
		name = "Desktop (HDD): ROOT"
	),
	plot.dataDrawable(
		logData,
		item = 'cpupct',
		keyList = [ "P2x%dEKPhdd" % threads for threads in range(1,5,1) ],
		indexList = range(2,9,2),
		name = "Desktop (HDD): Proof 2"
	),
	#plot.dataDrawable(
		#logData,
		#item = 'cpupct',
		#keyList = [ "P8x1EKPhdd" ],
		#indexList = [ 8 ],
		#name = "Desktop (HDD): Proof 8"
	#),
	plot.dataDrawable(
		logData,
		item = 'cpupct',
		keyList = [ "R1x%dEKPssd" % threads for threads in range(1,9) ],
		name = "Desktop (SSD): ROOT"
	),
	plot.dataDrawable(
		logData,
		item = 'cpupct',
		keyList = [ "P2x%dEKPssd" % threads for threads in range(1,5,1) ],
		indexList = range(2,9,2),
		name = "Desktop (SSD): Proof 2"
	),
	#plot.dataDrawable(
		#logData,
		#item = 'cpupct',
		#keyList = [ "P8x1EKPssd" ],
		#indexList = [ 8 ],
		#name = "Desktop (SSD): Proof 8"
	#)
]

title=plot.plotTitle(r"ROOT best case")
# mb/s/core
mbpspclabels = plot.plotAxisLabels(
	xlabel = "Cores",
	ylabel = "Throughput",
	yunit = "MB/S/Core"
	)
mbpspcRanges = plot.plotAxesRange(
	xrange = [0.5, 8.5],
	yrange = [0.0, 40.0]
	)
# cpu pct
cpupctlabels = plot.plotAxisLabels(
	xlabel = "CPU",
	ylabel = r"CPU Usage",
	yunit = r"\%"
	)
cpupctRanges = plot.plotAxesRange(
	xrange = [0.5, 8.5],
	yrange = [0.0, 100.0]
	)

plot.makePlot(
	'mbpspc.png',
	drawables = mbpspcDrawables,
	size = (4*1.5,3*1.5),
	plotFeatures=[title, mbpspclabels, mbpspcRanges],
	)
PU.resetGetter()
plot.makePlot(
	'cpupct.png',
	drawables = cpupctDrawables,
	plotFeatures=[title, cpupctlabels, cpupctRanges],
	)