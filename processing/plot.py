#!/usr/bin/python

from __future__ import print_function

import os
from pprint import pprint

import matplotlib.pyplot as plt
import matplotlib as mpl

from rtmlib.plotUtils import getIntensityCMap, getColorName, getMarkerName, PtToInch

class baseDrawable(object):
	def getXList(self):
		"""return list of x-values"""
		raise NotImplementedError
	def getXErrList(self):
		"""return list of x-errors"""
		raise NotImplementedError
	def getYList(self):
		"""return list of y-values"""
		raise NotImplementedError
		"""return list of y-errors"""
	def getYErrList(self):
		raise NotImplementedError

	def getLegend(self, **defOptions):
		"""return (legend drawable, legend label)"""
		raise NotImplementedError
	def getColor(self):
		raise NotImplementedError

	def draw(self, canvas, plot, **defOptions):
		"""add Drawable to plot"""
		raise NotImplementedError

class functionDrawable(baseDrawable):
	markershapeDefault = ' '
	markersizeDefault = 0
	def __init__(self, drawFunc, xRange, sampleInterval = 1.0, samplePoints = [], **style):
		self._drawFunc = drawFunc
		self._xRange = xRange
		self.style = style
		if samplePoints:
			self._samplePoints = samplePoints
		else:
			self._samplePoints = [ sampleInterval*mod+xRange[0] for mod in range(int((xRange[1]-xRange[0])/sampleInterval)) ]
		
	def getXList(self):
		return self._samplePoints
	def getXErrList(self):
		return [0] * len(self.getXList())
	def getYList(self):
		try:
			return self._yPoints
		except AttributeError:
			self._yPoints = [ self._drawFunc(dPoint) for dPoint in self.getXList() ]
			return self._yPoints
	def getYErrList(self):
		return [0] * len(self.getYList())

	def getLegend(self, **defOptions):
		"""return (legend drawable, legend label)"""
		raise NotImplementedError
	def getColor(self):
		try:
			return self._color
		except AttributeError:
			self._color = self.style.get("color", getColorName())
			return self._color

	def draw(self, canvas, plot, **defOptions):
		plot.errorbar(
			self.getXList(),
			self.getYList(),
			lw        = self.style.get("linewidth", defOptions.get("linewidth",2)),
			ls        = self.style.get("linestyle", defOptions.get("linestyle","--")),
			capsize   = self.style.get("capsize", defOptions.get("capsize",self.markersizeDefault)),
			fmt       = '--',
			color     = self.getColor(),
			alpha     = self.style.get("alpha", defOptions.get("alpha",1.)),
			markersize= self.style.get("markersize", defOptions.get("markersize",self.markersizeDefault)),
			label     = self.style.get("label", defOptions.get("labell","")),
			drawstyle = self.style.get("drawstyle", defOptions.get("drawstyle","default")),
			)

class dataDrawable(baseDrawable):
	"""Drawable for an item of chained data keys"""
	markershapeDefault = ' '
	markersizeDefault = 6
	def __init__(self, data, item, keyList, indexList = False, name = False, **style):
		# defaults
		if not indexList:
			indexList = range(1,len(keyList) + 1)
		if not name:
			name = "%s (%s-%s)" % (item, keyList[0], keyList[-1])
		self.data = data
		self.item = item
		self.keyList = keyList
		self.indexList = indexList
		self.name = name
		self.style = style
	# individual data access
	def getXList(self):
		return self.indexList
	def getXErrList(self):
		return [0] * len(self.getYList())
	def getYList(self):
		try:
			return [ self.data[key][self.item][0] for key in self.keyList ]
		except KeyError:
			print("Object", self, "only contains this data:")
			pprint(sorted(self.data.keys()))
			raise
	def getYErrList(self):
		return [ self.data[key][self.item][1] for key in self.keyList ]
	# meta providers
	def getLegend(self, **defOptions):
		return (
			mpl.lines.Line2D(
				[0, 0], [-1, 1],
				color  = self.getColor(),
				ls     = self.style.get("linestyle", defOptions.get("linestyle","-")),
				lw     = 1.25,
				marker = self.getMarker(),
				markersize = self.style.get("capsize", defOptions.get("capsize",self.markersizeDefault))
				),
			self.name
			)
	def getColor(self):
		try:
			return self._color
		except AttributeError:
			self._color = self.style.get("color", getColorName())
			return self._color
	def setColor(self, color):
		self._color = color
	def getMarker(self):
		try:
			return self._marker
		except AttributeError:
			self._marker = self.style.get("marker", getMarkerName())
			return self._marker
	# implementors
	def draw(self, canvas, plot, **defOptions):
		plot.errorbar(
			self.getXList(),
			self.getYList(),
			yerr      = self.getYErrList(),
			lw        = self.style.get("linewidth", defOptions.get("linewidth",2)),
			ls        = self.style.get("linestyle", defOptions.get("linestyle"," ")),
			capsize   = self.style.get("capsize", defOptions.get("capsize",self.markersizeDefault)),
			fmt       = '-',
			color     = self.getColor(),
			alpha     = self.style.get("alpha", defOptions.get("alpha",1.)),
			marker    = self.getMarker(),
			markersize= self.style.get("markersize", defOptions.get("markersize",self.markersizeDefault)),
			label     = self.style.get("label", defOptions.get("labell","")),
			drawstyle = self.style.get("drawstyle", defOptions.get("drawstyle","steps-mid")),
			)

class dataThresholdDrawable(baseDrawable):
	def __init__(self, data, item, keyList, threshold = 0.0, indexList = False, name = False, **style):
		# defaults
		if not indexList:
			indexList = range(1,len(keyList) + 1)
		if not name:
			name = "%s (%s-%s)" % (item, keyList[0], keyList[-1])
		self.data      = data
		self.item      = item
		self.threshold = threshold
		self.keyList   = keyList
		self.indexList = indexList
		self.name      = name
		self.style     = style
	def getXList(self):
		return self.indexList
	def getXErrList(self):
		return [0] * len(self.getYList())
	def getYList(self):
		try:
			return self.Ydata
		except AttributeError:
			try:
				dataY = [ self.data[key][self.item][0] for key in self.keyList ]
			except KeyError:
				print("Object", self, "only contains this data:")
				pprint(self.data.keys())
				raise
			self.Ydata = [0]
			for index in range(1,len(dataY)):
				if dataY[index] > self.threshold and dataY[index-1] < self.threshold:
					self.Ydata.append(1)
					self.Ydata[index-1] = -1
				if dataY[index] < self.threshold and dataY[index-1] > self.threshold:
					self.Ydata.append(-1)
					self.Ydata[index-1] = 1
				else:
					self.Ydata.append(0)
			return self.Ydata
	def getYErrList(self):
		return [0] * len(self.getYList())
	# meta providers
	def getColor(self):
		try:
			return self._color
		except AttributeError:
			self._color = self.style.get("color", getColorName())
			return self._color
	def setColor(self, color):
		self._color = color
	def getMarker(self):
		try:
			return self._marker
		except AttributeError:
			self._marker = self.style.get("marker", getMarkerName())
			return self._marker
	# implementors
	def draw(self, canvas, plot, **defOptions):
		xPos = self.getXList()
		yPos = self.getYList()
		for index in range(len(xPos)-1):
			if yPos[index] * yPos[index+1] == -1:
				plot.axvline( x = (xPos[index]+xPos[index+1])/2.0, ymin = 0, ymax = 1, color = self.getColor(), **self.style)

class dataStripeDrawable(dataDrawable):
	pass

class basePlotFeature():
	def add(self, canvas, plot, **meta):
		raise NotImplementedError

class plotAxisLine(basePlotFeature):
	def __init__(self, orient = 'h', coord=0, span=[0,1], **kwargs):
		self.orient   = str(orient).lower()
		self.coord  = coord
		self.span   = span
		self.kwargs = kwargs
	def add(self, canvas, plot, **meta):
		if self.orient in [ 'v', '1', 1]:
			plot.axvline( x = self.coord, ymin = self.span[0], ymax = self.span[1], **self.kwargs)
		else:
			plot.axhline( y = self.coord, xmin = self.span[0], xmax = self.span[1], **self.kwargs)

class plotGrid(basePlotFeature):
	def __init__(self, alpha = 0.25, color = '0', linestyle = '--', linewidth = 1, zorder = 0):
		self.alpha = alpha
		self.color = color
		self.linestyle = linestyle
		self.linewidth = linewidth
		self.zorder = zorder

	def add(self, canvas, plot, **meta):
		plot.grid(
			alpha = self.alpha,
			color = self.color,
			linestyle = self.linestyle,
			linewidth = self.linewidth,
			zorder = self.zorder,
			)

class plotLogAxes(basePlotFeature):
	def __init__(self, axes = 'y'):
		self.axes = axes.lower()
	def add(self, canvas, plot, **meta):
		if 'x' in self.axes:
			plot.set_xscale('log')
		if 'y' in self.axes:
			plot.set_yscale('log')

class plotAxesRange(basePlotFeature):
	def __init__(self, xrange = [], yrange = []):
		self.xrange = xrange
		self.yrange = yrange
	def add(self, canvas, plot, **meta):
		if self.xrange:
			plot.set_xlim(self.xrange)
		if self.yrange:
			plot.set_ylim(self.yrange)

class plotAxisLabels(basePlotFeature):
	def __init__(self, xlabel = 'X', xunit = None, ylabel = 'Y', yunit = None):
		self.xlabel = xlabel
		self.xunit = xunit
		self.ylabel = ylabel
		self.yunit = yunit
	def add(self, canvas, plot, **meta):
		xlabel = self.xlabel
		if self.xunit is not None:
			xlabel = "%s [%s]" % (self.xlabel, self.xunit)
		ylabel = self.ylabel
		if self.yunit is not None:
			ylabel = "%s [%s]" % (self.ylabel, self.yunit)
		plot.set_xlabel(xlabel, va="top", ha = "right", x = 1)
		plot.set_ylabel(ylabel, va="top", ha = "right", y = 1)

class plotTitle(basePlotFeature):
	def __init__(self, title):
		self.title = title
	def add(self, canvas, plot, **meta):
		pos = meta["titleAnchor"]
		if [ char for char in "ypqgj" if char in self.title ]:
			canvas.text(meta["titleAnchor"][0], meta["titleAnchor"][2], self.title, ha='left', va='bottom')
		else:
			canvas.text(meta["titleAnchor"][0], meta["titleAnchor"][1], self.title, ha='left', va='bottom')

def makePlot(outputName, drawables = [], plotFeatures = [], size = (8.0, 6.0), dpi = 200, style = {}):
	fontsize = mpl.rcParams.get('font.size')
	descentRatio = mpl.textpath.TextPath((0,0), 'g').get_extents().height / mpl.textpath.TextPath((0,0), 'a').get_extents().height
	# calculate plot area: left, bottom, width, height of plot
	# border: 2xLabel+3xChar, 2xLabel+1xTicks
	plotExtent = [
		(2*PtToInch(fontsize) + 3*PtToInch(fontsize))/size[0],
		(2*PtToInch(fontsize) + 1*PtToInch(fontsize))/size[1],
		(size[0] - (4*PtToInch(fontsize) + 3*PtToInch(fontsize)))/size[0],
		(size[1] - (4*PtToInch(fontsize) + 1*PtToInch(fontsize)))/size[1],
		]
	titleAnchor = (
			(2*PtToInch(fontsize) + 3*PtToInch(fontsize))/size[0],
			(size[1]-PtToInch(fontsize)*(1.5))/size[1],
			(size[1]-PtToInch(fontsize)*(0.5+descentRatio))/size[1],
		)
	# store for legend data
	legendContent = []
	# container for plot drawing data for helper functions
	meta = {
		"fontsize"		: fontsize,
		"titleAnchor"	: titleAnchor,
		"legendContent"	: legendContent,
		"plotsize"		: size,
		}
	
	# create canvas and plot area
	canvas = plt.figure(figsize = size)
	plot = canvas.add_subplot(111, position = plotExtent)
	
	# set plot features
	for feature in plotFeatures:
		feature.add(canvas, plot, **meta)
	# add drawables to plot
	for drawable in drawables:
		drawable.draw(canvas, plot)
		try:
			legendContent.append(drawable.getLegend())
		except NotImplementedError:
			pass
	# add legend
	legend = plot.legend(
		*zip(*legendContent),
		numpoints = 1,
		loc       = style.get("legLocation", "best"),
		prop      = {
					"size" : 1*fontsize
					}
		)
	
	if os.path.dirname(outputName) and not os.path.exists(os.path.dirname(outputName)):
		os.makedirs(os.path.dirname(outputName))
	# safe to file
	canvas.savefig(outputName, dpi = dpi)




