#!/bin/bash

### TODO: Other copy protocols

for shFile in $(shopt -s nullglob; echo {storageTestPrimer*.sh,utility.sh})
do
	source $shFile
done

#######
# Helper functions
#######
# trap for cleanup on exit
cleanUp () {
	timeMsg "Exiting script: ${MyName}"
	if [[ ${#__CLEANLIST_LCL[@]} -gt "0" ]]
	then
		TMP_i=1
		echoMsg "Removing ${#__CLEANLIST_LCL[@]} files..."
		for RMTHING in ${__CLEANLIST_LCL[@]}
		do
			if ! [[ -e ${RMTHING} ]]; then
				echoMsg "${TMP_i}/${#__CLEANLIST_LCL[@]} : File no longer exists"
			elif [[ -d ${RMTHING} ]]; then
				( checkMsg "rm -r ${RMTHING}" "${TMP_i}/${#__CLEANLIST_LCL[@]} : Removing directory" )  || ( warnMsg "Could not remove ${RMTHING}." )
			elif [[ -f ${RMTHING} ]]; then
				( checkMsg "rm    ${RMTHING}" "${TMP_i}/${#__CLEANLIST_LCL[@]} : Removing file" )       || ( warnMsg "Could not remove ${RMTHING}." )
			else
				( warnMsg "${TMP_i}/${#__CLEANLIST_LCL[@]} : Could not remove ${RMTHING}." )
			fi
			TMP_i=$(( $TMP_i + 1 ))
		done
	fi
	timeMsg "Exited: ${MyName}"
}
trap cleanUp EXIT

#######
# Variables and Containers
#######
# internals
InitPwd=$(pwd)
InitDate="$(date '+%y%m%d.%H%M%S')"
MyName=$(basename ${BASH_SOURCE[0]})
MyRANDOM=$RANDOM
# defaults
STMR_ID="NONE"
STMR_Log="log.STRM.${InitDate}.out"

STMR_Sources=()
STMR_Dest="/dev/null"
STMR_Command='dd'

STMR_NInstances='1'
STMR_TimeOut='0'

STMR_SampleSizeKB="off"
STMR_SampleSource="/dev/zero"
STMR_SampleSame="off"

STMR_ExtraArgs=""

#######
# Command Line Interface
#######
while [[ $1 ]]
do
	case $1 in
		-h|--help)
			echo "Script for performing a storage performance test"
			echo "   running multiple storage accesses in parallel"
			echo
			echo "Implicit file requirement:"
			echo "	utility.sh   utility functions"
			echo ""
			echo "Logging Options:"
			echo '-i|--identifier <ID>          assign run identifier <ID> in logs'
			echo '-o|--out-file <FILE>          write results to file <FILE>'
			echo
			echo "Testing Options:"
			echo '-s|--source <SRC> [SRC [...]]  glob/file/dev/... to read data from'
			echo '                               each item is assigned to one instance'
			echo "-d|--destination <DEST>        dirname to copy data to              - def: $STMR_Dest"
			echo
			echo "-c|--instance-count <C>        number of simultaneous instances     - def: $STMR_NInstances"
			echo "-t|--time-out <TO>             end transfer after <TO> seconds"
			echo "-a|--arguments <ARGS>          arguments to pass to copy program"
			echo
			echo "Local Sample Creation Options:"
			echo "-S|--sample-size <SIZE>       sample will be <SIZE> KB             - def: off"
			echo "-R|--sample-resource <DEV>    sample will be created from <DEV>    - def: $STMR_SampleSource"
			echo "-U|--sample-unique [$util_TrueList]        use same sample for all instances    - def: $STMR_SampleSame"
			echo
			echo "Return codes:"
			echo "   $UTIL_RETCODE_GENERIC  ==  Internal error"
			echo "   $UTIL_RETCODE_PERMISSION  ==  Permission Denied"
			echo "   $UTIL_RETCODE_NOTFOUND  ==  Not found"
			echo "   $UTIL_RETCODEUSAGE  ==  Input error"
			exit 0
		;;
		-i|--identifier)
			STMR_ID=$2
			shift 2
		;;
		-o|--out-file)
			STMR_Log=$2
			shift 2
		;;

		-s|--source)
			while [[ -n "$2" ]] && [[ "$2" != "-"* ]]; do
				STMR_Sources+=($2)
				shift 1
			done
			shift 1
		;;
		-d|--destination)
			STMR_Dest=$2
			shift 2
		;;

		-c|--instance-count)
			STMR_NInstances=$2
			shift 2
		;;
		-t|--time-out)
			STMR_TimeOut=$2
			shift 2
		;;
		-a|--arguments)
			STMR_ExtraArgs=$2
			shift 2
		;;

		-S|--sample-size)
			STMR_SampleSizeKB=$2
			shift 2
		;;
		-R|--sample-resource)
			STMR_SampleSource=$2
			shift 2
		;;
		-U|--sample-unique)
			if [[ -z "$2" ]] || [[ "$2" == "-"* ]]; then
				STMR_SampleSame=true
				shift 1
			else
				STMR_SampleSame=$(util_ArgIsTrue $2)
				shift 2
			fi
		;;

		*)
			fail "Found unexpected CLI arg: $1"
			exit 1
		;;
	esac
done

timeMsg "Starting script: ${MyName}"

######
# Finalize variable initialisation
STMR_SampleSame=$(util_ArgIsTrue $STMR_SampleSame)
######
STMR_KEY="${MyName}.${STMR_ID}.${InitDate}.${MyRANDOM}"
STMR_FAILFILE="${InitPwd}/runfail.$STMR_KEY"
STMR_DATAFILE="${InitPwd}/rundata.$STMR_KEY"
STMR_TMPSAMPLE="${InitPwd}/TMPSAMPLE.${STMR_KEY}"
#######
# Files to be removed
__CLEANLIST_LCL=( $STMR_FAILFILE $STMR_DATAFILE )
__CLEANLIST_RMT=()

# Resolve internal variable logic
if [[ ${#STMR_Sources[@]} -eq "0" ]]
then
	fail "No source(s) specified"
	exit $UTIL_RETCODEUSAGE
fi


# create SOURCE/DEST list
###########
if [[ ${#STMR_Sources[@]} -gt "1" ]] && [[ ${STMR_NInstances} -gt "1" ]]
then
	if [[ ${#STMR_Sources[@]} -lt $STMR_NInstances ]]
	then
		warnMsg "Source count (${#STMR_Sources[@]}) insufficient for requested instances ($STMR_NInstances)"
		warnMsg "Instances will loop through source list"
		#STMR_NInstances=${#STMR_Sources[@]}
	elif [[ ${#STMR_Sources[@]} -gt $STMR_NInstances ]]
	then
		warnMsg "Instance count ($STMR_NInstances) insufficient for provided sources (${#STMR_Sources[@]})"
		warnMsg "Truncating sources to instance count"
		STMR_Sources=${STMR_Sources[@]:0:$STMR_NInstances}
	fi
fi

# create sample if desired
if [[ "$STMR_SampleSizeKB" -gt "0" ]]
then
	timeMsg "Creating sample(s)..."
	# expand target list to instance count if requested
	if [[ ${#STMR_Sources[@]} -eq "1" ]] && ! [[ ${STMR_SampleSame} ]]
	then
		for Index in $(seq 2 $STMR_NInstances)
		do
			STMR_Sources+=($STMR_Sources)
		done
	fi
	# ensure temporary file names
	TMP_List=()
	TMP_i=1
	for baseSourceFile in ${STMR_Sources[@]}
	do
		TMP_List+=("${baseSourceFile}.tmp.${STMR_KEY}.${TMP_i}")
		TMP_i=$(( $TMP_i + 1 ))
	done
	# recreate sample list with random samples
	STMR_Sources=()
	TMP_i=1
	echoMsg "Will create ${#TMP_List[@]} sample file(s)..."
	for sampleFile in ${TMP_List[@]}
	do
		# ensure it's in an accessible folder
		checkMsg "mkdir -p $(dirname $sampleFile)" "Directory $(dirname $sampleFile) is a locally accessible test folder."
		# create sample
		STMR_Sources+=($sampleFile)
		__CLEANLIST_LCL+=($sampleFile)
		assertMsg "dd bs=1024 count=$STMR_SampleSizeKB if=\"${STMR_SampleSource}\" of=\"${sampleFile}\" conv=fdatasync" "Creating random sample ${TMP_i}/${#TMP_List[@]}"
		TMP_i=$(( $TMP_i + 1 ))
	done
fi
# create target list
############
STMR_DestFiles=()
if [[ "$STMR_Dest" == */dev/null ]]
then
	STMR_DestFiles+=(${STMR_Dest})
else
	for Index in $(seq 1 $STMR_NInstances)
	do
		STMR_DestFiles+=("${STMR_Dest}/${STMR_KEY}.I${Index}")
	done
	__CLEANLIST_LCL+=(${STMR_DestFiles[@]})
fi

#echo -e "{" >> $STMR_DATAFILE
echo -e "
\t,\"__comment\" : {
\t\t\"script\"    : \"${MyName}\",
\t\t\"instances\" : \"$STMR_NInstances\",
\t\t\"ID\"        : \"${STMR_ID}\",
\t\t\"hostname\"  : \"$(hostname)\",
\t\t\"sources\"   : \"${STMR_Sources[@]}\",
\t\t\"dests\"     : \"${STMR_DestFiles[@]}\",
\t\t\"generator\" : \"${STMR_SampleSource}\"
\t}" >> $STMR_DATAFILE

# run tests
timeMsg "Beginning test: $STMR_NInstances simultaneous instances."
for InstanceID in $(seq 1 $STMR_NInstances);do
	__CLEANLIST_LCL+=(${STMR_DATAFILE}_${InstanceID})
	# launch tests in background
	(
		Instance_Source=${STMR_Sources[$(( ( $InstanceID - 1 ) % ${#STMR_Sources[@]} ))]}
		Instance_Dest=${STMR_DestFiles[$(( ( $InstanceID - 1 ) % ${#STMR_DestFiles[@]} ))]}
		Instance_LogFile="${InitPwd}/tmplog.${STMR_KEY}.I${InstanceID}.R${REPEAT}.out"
		[[ "${Instance_Dest}" != */dev/null ]] && ARG_CONV="conv=fdatasync"
		# perform copy
		dd bs=128M if=${Instance_Source} of=${Instance_Dest} iflag=direct $ARG_CONV  2>> $Instance_LogFile 1>&2 &
		copyPid=$!
		# timeout: force write and exit
		if [[ "$Instance_Source" == "/dev/random" ]] || [[ "$Instance_Source" == "/dev/urandom" ]] || [[ "$Instance_Source" == "/dev/zero" ]] || [[ "$STMR_TimeOut" -gt "0" ]]; then
			(
			sleep ${STMR_TimeOut:-"600"}
			kill -USR1 $copyPid 2>/dev/null 1>&2
			kill -2 $copyPid 2>/dev/null 1>&2
			sleep 2
			kill -15 $copyPid 2>/dev/null 1>&2
			) &
		fi
		# wait for copy completion before continuing
		wait $copyPid 2>/dev/null 1>&2
		RETVAL=$?
		Instance_Nickname="C1x${STMR_NInstances}"
		Instance_Fullname="${InstanceID}@${Instance_Nickname}"
		if [[ $RETVAL -ne 0 ]];then
			# catch SIGINT from timeout
			if  [[ $RETVAL -eq $(( 128 + 15 )) ]];then
				echoMsg "Instance ${InstanceID} timeout (${STMR_TimeOut:-"600"}s)"
			else
				warnMsg "Instance ${InstanceID} failed with $RETVAL"
				cat $Instance_LogFile >&2
				rm ${Instance_LogFile}
				touch $STMR_FAILFILE
				exit $RETVAL
			fi
		fi
		if ! [[ -s $Instance_LogFile ]]
		then
			warnMsg "Instance ${InstanceID} failed to produce output"
			rm ${Instance_LogFile}
			touch $STMR_FAILFILE
			exit $UTIL_RETCODE_GENERIC
		fi
		echoMsg "Instance ${InstanceID} finished succesfully"
		# get statistics
		VolumeB=$(tail -1 $Instance_LogFile | awk '{print $1}')
		DurationS=$(tail -1 $Instance_LogFile | awk '{print $6}')
		dd_AvgRate=$(tail -1 $Instance_LogFile | awk '{print $8$9}')
		AvgRateK=$(awk "BEGIN{print 1024 * $VolumeB / $DurationS }")
		rm ${Instance_LogFile}
		success "() ${Instance_Fullname} ${AvgRateK} KB/S [ VB: ${VolumeB} Ds: ${DurationS} DD: ${dd_AvgRate} ]"
		echo -e "\t,\"$Instance_Nickname[$(date '+%y%m%d.%H%M%S')]\" : {
\t\t\"fullname\"  : \"${Instance_Fullname}\",
\t\t\"nickname\"  : \"${Instance_Nickname}\",
\t\t\"instances\" : \"$STMR_NInstances\",
\t\t\"ID\"        : \"${STMR_ID}\",
\t\t\"hostname\"  : \"$(hostname)\",

\t\t\"source\"    : \"${Instance_Source}\",
\t\t\"dest\"      : \"${Instance_Dest}\",

\t\t\"volume\"    : \"${VolumeB}\",
\t\t\"duration\"  : \"${DurationS}\",
\t\t\"avgrateK\"   : \"${AvgRateK}\",
\t\t\"raw\"       : { \"ddRate\" : \"${dd_AvgRate}\" }
\t}" >> "${STMR_DATAFILE}_${InstanceID}"
	) &
done
# wait for tests to finish
wait
timeMsg "All instances have exited..."
# save data only if no errors occured
if [ -f "$RTMR_FAILFILE" ]
then
	fail "At least one instance exited abnormally"
	echoMsg "Ignoring Data"
else
	success "All instances exited successfully"
	echoMsg "Storing Data"
	# pass instance log data sequentially to main log to avoid collisions
	for InstanceID in $(seq 1 $STMR_NInstances);do
		cat "${STMR_DATAFILE}_${InstanceID}" >> "${STMR_DATAFILE}"
	done
fi

#echo "}" >> ${STMR_DATAFILE}

if [[ -s ${STMR_DATAFILE} ]]
then
	cat ${STMR_DATAFILE} >> $STMR_Log
	success "Saving Data"
	exit 0
else
	fail "No Data generated"
	exit $UTIL_RETCODE_GENERIC
fi
	






















