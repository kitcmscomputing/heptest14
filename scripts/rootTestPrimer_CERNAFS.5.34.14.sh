#!/bin/bash

echo "Primer: CERN AFS (ROOT 5.34.14)"

. /afs/cern.ch/sw/lcg/external/gcc/4.7/x86_64-slc6/setup.sh /afs/cern.ch/sw/lcg/contrib
cd /afs/cern.ch/sw/lcg/app/releases/ROOT/5.34.14/x86_64-slc6-gcc47-opt/root/
. /afs/cern.ch/sw/lcg/app/releases/ROOT/5.34.14/x86_64-slc6-gcc47-opt/root/bin/thisroot.sh
cd - 2>/dev/null 1>&2
