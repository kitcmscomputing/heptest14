#!/bin/bash

for shFile in utility.sh
do
	source $shFile
done

# trap for cleanup on exit
cleanUp () {
	[ -d "${INITPWD}/RTMM_${RTMM_KEY}" ] && ( rm -r "${INITPWD}/RTMM_${RTMM_KEY}"; report "Removing working directory" )
	[ -d "${RTMM_SAMPLEDIR}/RTMM_${RTMM_KEY}" ] && ( rm -r "${RTMM_SAMPLEDIR}/RTMM_${RTMM_KEY}"; report "Removing sample directory" )
	timeMsg "Exited"
}
trap cleanUp SIGINT EXIT

INITPWD=$(pwd)
INITDATE="$(date '+%y%m%d.%H%M%S')"
# set defaults
RTMM_RANDOM=$RANDOM
RTMM_KEY="${INITDATE}.${RTMM_RANDOM}"
RTMM_ID="NONE"
RTMM_LOG="NONE"
RTMM_SAMPLEDIR="${INITPWD}/samples"
RTMM_SAMPLESAME="0"
RTMM_NMAXCORES="1"
RTMM_NMAXPROOF="1"
RTMM_NREPEATS="1"

# parse command line options
while [[ $1 ]]
do
	case $1 in
		-h|--help)
			echo "Script for launching a set of ROOT performance tests"
			echo "   running several ROOT instances in parallel"
			echo ""
			echo "Implicit file requirement:"
			echo "   rootTestCore.tgz           containing ROOT macro RunMuSelector"
			echo "   rootTestSample*.tgz        containing muon sample"
			echo "   rootTest_MultiRun.sh       performing each individual test"
			echo "   utility.sh                 implementation helper scripts"
			echo ""
			echo "Logging Options:"
			echo '-i|--identifier ID            run identifier ID to assign in logs'
			echo '-o|--out-file FILE            file FILE to write results to'
			echo
			echo "Testing Options:"
			echo '-c|--cpu-count C              number of cores to use at most'
			echo '-p|--proof-limit P            number of proof workers to use at most ( P<=1 : Off)'
			echo '-n|--repeats N                number of complete test repetitions'
			echo '-r|--runs-per-sample R        number of passes per sample instance'
			echo
			echo "Sample Options"
			echo '-s|--sample-destination DEST  place samples in subfolder of DEST for each test'
			echo '-g|--sample-glob SOURCE       use sample(s) matched by SOURCE for each test'
			echo '-m|--multiuse-sample YES      use the same sample for all instances (Def: off)'
			exit 0
		;;
		-i|--identifier)
			RTMM_ID=$2
			shift 2
		;;
		-o|--out-file)
			RTMM_LOG=$2
			shift 2
		;;
		-s|--sample-destination)
			RTMM_SAMPLEDIR=$2
			shift 2
		;;
		-g|--sample-glob)
			RTMM_SAMPLEGLOB=$2
			shift 2
		;;
		-m|--multiuse-sample)
			case $2 in
				Y|T|1|Yes|True|On|YES|TRUE|ON)
				RTMM_SAMPLESAME="1"
				;;
			esac
			shift 2
		;;
		-c|--cpu-count)
			RTMM_NMAXCORES=$2
			shift 2
		;;
		-p|--proof-limit)
			RTMM_NMAXPROOF=$2
			shift 2
		;;
		-n|--repeats)
			RTMM_NREPEATS=$2
			shift 2
		;;
		-r|--runs-per-sample)
			RTMM_NRunsPerSample=$2
			shift 2
		;;
		*)
			fail "Found unexpected CLI arg: $1"
			exit 1
		;;
	esac
done

timeMsg "Initializing tests..."

if [ "$RTMM_LOG" == "NONE" ]
then
	if [ "$RTMM_ID" == "NONE" ]
	then
		RTMM_LOG="log.RTMM.${RTMM_KEY}.out"
	else
		RTMM_LOG="log.${RTMM_ID}.${INITDATE}.out"
	fi
fi

case $RTMM_LOG in
	/*)
	;;
	*)
	RTMM_LOG="$(pwd)/${RTMM_LOG}"
	;;
esac
echoMsg "Writing to ${RTMM_LOG}"

# Construct constant CLI argument portion
# output File
SINGLERUNARG="-o $RTMM_LOG"
# ID
if [ -n "$RTMM_ID" ]
then
	SINGLERUNARG+=" -i $RTMM_ID"
fi
# Sample Source
if [ -n "$RTMM_SAMPLEGLOB" ]
then
	SINGLERUNARG+=" -g $RTMM_SAMPLEGLOB"
fi
# Passes Per Sample
if [ -n "$RTMM_NRunsPerSample" ]
then
	SINGLERUNARG+=" -r $RTMM_NRunsPerSample"
fi

echo -e "{
\"type\"  : \"rootTestMulti\",
\"host\"  : \"$(hostname)\",
\"run\"   : {
\t\"ID\"         : \"${RTMM_ID}\",
\t\"maxcores\"   : ${RTMM_NMAXCORES},
\t\"maxproof\"   : ${RTMM_NMAXPROOF},
\t\"samesample\" : \"${RTMM_SAMPLESAME}\",
\t\"sampledir\"  : \"${RTMM_SAMPLEDIR}\",
\t\"initdate\"   : \"${INITDATE}\",
\t\"startdate\"  : \"$(date '+%y%m%d.%H%M%S')\"
},
\"data\"  : {" > $RTMM_LOG
for REPEAT in $(seq 1 $RTMM_NREPEATS)
do
	for RTM_INSTANCES in $(seq 1 $RTMM_NMAXCORES)
	do
		echo -e "\t\"__comment\"  : \"I:${RTM_INSTANCES} @:$(date '+%y%m%d.%H%M%S')\"," >> $RTMM_LOG
			echo -e "\t\"__comment\"  : \"R1x${RTM_INSTANCES} @:$(date '+%y%m%d.%H%M%S')\"," >> $RTMM_LOG
			(
			timeMsg "Dispatching: ${RTM_INSTANCES} instances with 1 root thread (Run: ${REPEAT})"
			SESSION_FOLDER="${INITPWD}/RTMM_${RTMM_KEY}/R1x${RTM_INSTANCES}"
			mkdir -p $SESSION_FOLDER
			assertMsg "cp $(shopt -s nullglob; echo utility.sh rootTest_MultiRun.sh rootTestCore.tgz rootTestPrimer*.sh rootTestSample*.tar) $SESSION_FOLDER" "Preparing Session Folder"
			cd $SESSION_FOLDER
			./rootTest_MultiRun.sh $SINGLERUNARG -c $RTM_INSTANCES -s "$RTMM_SAMPLEDIR/RTMM_${RTMM_KEY}/R1x${RTM_INSTANCES}"
			cd -
			assertMsg "rm -r $SESSION_FOLDER" "Removing test folder..."
			)
		_MAXPROOF=$(awk "BEGIN{print int($RTMM_NMAXCORES/$RTM_INSTANCES) }")
		for RTM_THREADS in $(seq 2 $(( $RTMM_NMAXPROOF > $_MAXPROOF ? $_MAXPROOF : $RTMM_NMAXPROOF )) )
		do
			echo -e "\t\"__comment\"  : \"P${RTM_THREADS}x${RTM_INSTANCES} @:$(date '+%y%m%d.%H%M%S')\"," >> $RTMM_LOG
			(
			timeMsg "Dispatching: ${RTM_INSTANCES} instances with ${RTM_THREADS} proof threads (Run: ${REPEAT})"
			SESSION_FOLDER="${INITPWD}/RTMM_${RTMM_KEY}/P${RTM_THREADS}x${RTM_INSTANCES}"
			mkdir -p $SESSION_FOLDER
			assertMsg "cp $(shopt -s nullglob; echo utility.sh rootTest_MultiRun.sh rootTestCore.tgz rootTestPrimer*.sh rootTestSample*.tar) $SESSION_FOLDER" "Preparing Session Folder"
			cd $SESSION_FOLDER
			./rootTest_MultiRun.sh $SINGLERUNARG -t $RTM_THREADS -c $RTM_INSTANCES -s "$RTMM_SAMPLEDIR/RTMM_${RTMM_KEY}/P${RTM_THREADS}x${RTM_INSTANCES}"
			cd -
			assertMsg "rm -r $SESSION_FOLDER" "Removing test folder..."
			)
		done
	done
done
echo -e "\t\"__comment\"  : \"I:${INITDATE} @:$(date '+%y%m%d.%H%M%S')\"" >> $RTMM_LOG
echo -e "}
}
" >> $RTMM_LOG

timeMsg "Done"
exit 0
