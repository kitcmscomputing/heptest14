#!/bin/bash

source utility.sh

# trap for cleanup on exit
cleanUp () {
	[ -d ${RTMR_SAMPLEDIR} ] && ( rm -r ${RTMR_SAMPLEDIR} ; report "Removing sample folder" )
	[ -d "${INITPWD}/testCore/" ] && ( rm -r "${INITPWD}/testCore/" ; report "Removing test folder" )
	[ -f "${INITPWD}/timing.T1.R1.out" ] && ( rm ${INITPWD}/timing.T*.out ; report "Removing log files" )
}
trap cleanUp EXIT

INITPWD=$(pwd)
# set defaults
RTMR_ID="NONE"
RTMR_LOG="log.RTRM.out"
RTMR_SAMPLEDIR="$(pwd)/samples"
RTMR_SAMPLEGLOB="rootTestSample*.tar"
RTMR_SAMPLESAME="0"
RTMR_NINSTANCES="1"
RTMR_NTHREADS=
RTMR_NRunsPerSample="1"

# parse command line options
while [[ $1 ]]
do
	case $1 in
		-h|--help)
			echo "Script for performing a ROOT performance test"
			echo "   running several ROOT instances in parallel"
			echo ""
			echo "Implicit file requirement:"
			echo "	rootTestCore.tgz	containing ROOT macro RunMuSelector"
			echo "	rootTestSample*.tgz	containing muon sample"
			echo ""
			echo "Logging Options:"
			echo '-i|--identifier ID            run identifier ID to assign in logs'
			echo '-o|--out-file FILE            file FILE to write results to'
			echo
			echo "Testing Options:"
			echo '-c|--instance-count C         number of ROOT instances to launch'
			echo '-t|--threads T                number of PROOF threads to use per instance'
			echo '                              Default: use bare ROOT (1 thread)'
			echo '-r|--runs-per-sample R        number of passes per sample instance'
			echo
			echo "Local Sample Access"
			echo '-s|--sample-destination DEST  place samples in subfolder of DEST for each test'
			echo '-g|--sample-glob SOURCE       use sample(s) matched by SOURCE for each test'
			echo '-m|--multiuse-sample YES      use the same sample for all instances (Def: off)'
			echo "DCache Sample Access"
			echo '-d|--sample-dcache DCACHE     use samples in DCache location DEST for each test'
			echo '                              as DCACHE_01, DCACHE_02, DCACHE_03, etc.'
			exit 0
		;;
		-i|--identifier)
			RTMR_ID=$2
			shift 2
		;;
		-o|--out-file)
			RTMR_LOG=$2
			shift 2
		;;
		-s|--sample-destination)
			RTMR_SAMPLEDIR=$2
			shift 2
		;;
		-g|--sample-glob)
			RTMR_SAMPLEGLOB=$2
			shift 2
		;;
		-m|--multiuse-sample)
			if [[ -z "$2" ]] || [[ "$2" == "-"* ]]; then
				RTMR_SAMPLESAME="1"
				shift 1
			else
				case $2 in
					Y|T|1|Yes|True|On|YES|TRUE|ON)
						RTMR_SAMPLESAME="1"
					;;
				esac
				shift 2
			fi
		;;
		-c|--instance-count)
			RTMR_NINSTANCES=$2
			shift 2
		;;
		-t|--threads)
			RTMR_NTHREADS=$2
			shift 2
		;;
		-t|--threads)
			RTMR_NTHREADS=$2
			shift 2
		;;
		-r|--runs-per-sample)
			RTMR_NRunsPerSample=$2
			shift 2
		;;
		*)
			fail "Found unexpected CLI arg: $1"
			exit 1
		;;
	esac
done

for shFile in $(shopt -s nullglob; echo rootTestPrimer*.sh)
do
	source $shFile && success "Initialized $shFile" || fail "Error with $shFile"
done

# Initialize
STARTDATE_PREPARE=`date +%s`
RTMR_FAILFILE="${INITPWD}/runfail.$RANDOM"
RTMR_INFOFILE="${INITPWD}/runinfo.$RANDOM"
RTMR_TIMEFILE="${INITPWD}/runtime.$RANDOM"
RTMR_LOGSFILE="${INITPWD}/runlogs.$RANDOM"

timeMsg "Preparing test..."
assertMsg "which root" "Checking for root executable"

# prepare samples first to allow proper linking
#check existence
SAMPLES=$(shopt -s nullglob; echo $RTMR_SAMPLEGLOB)
if [[ ${#SAMPLES[@]} -ne 1 ]]
then
	fail "Sample Glob $RTMR_SAMPLEGLOB did not match exactly one file"
fi
# unpack one instance...
mkdir -p "$RTMR_SAMPLEDIR/S1"
assertMsg "tar -xf $SAMPLES -C ${RTMR_SAMPLEDIR}/S1"	"Unpacking base sample"
# then reuse...
for SampleID in $(seq 2 $RTMR_NINSTANCES);do
	if [ "$RTMR_SAMPLESAME" -ne "1" ]
	then
		mkdir -p "${RTMR_SAMPLEDIR}/S${SampleID}"
		cp ${RTMR_SAMPLEDIR}/S1/* "${RTMR_SAMPLEDIR}/S${SampleID}"
		report "Sample $SampleID (Copy)"
	else
		ln -s "${RTMR_SAMPLEDIR}/S1" "${RTMR_SAMPLEDIR}/S${SampleID}"
		report "Sample $SampleID (Link)"
	fi
done

# unpack executable...
mkdir -p "testCore/T1"
assertMsg "tar -zxf rootTestCore.tgz -C ${INITPWD}/testCore/T1"		"Unpacking Macro"
# then reuse...
for InstanceID in $(seq 2 $RTMR_NINSTANCES);do
	mkdir -p "${INITPWD}/testCore/T${InstanceID}"
	cp ${INITPWD}/testCore/T1/* "${INITPWD}/testCore/T${InstanceID}"
	report "Macro $InstanceID (Copy)"
done
# link to samples
for InstanceID in $(seq 1 $RTMR_NINSTANCES);do
	ln -s "${RTMR_SAMPLEDIR}/S${InstanceID}" "${INITPWD}/testCore/T${InstanceID}/sample"
	report "Macro $InstanceID -> Sample $InstanceID (Link)"
done
timeMsg "Preparation time: $[ $(date +"%s") - $STARTDATE_PREPARE ] s"

# try to empty caches
sync
echo 3 > /proc/sys/vm/drop_caches && RTMR_CACHEDROP="true" || RTMR_CACHEDROP="false"

# run tests
for REPEAT in $(seq 1 $RTMR_NRunsPerSample)
do
(
	if [ -n "$RTMR_NTHREADS" ]
	then
		export rootTestProofWorkers=$RTMR_NTHREADS
		timeMsg "Beginning test: ${RTMR_NINSTANCES} simultaneous instances with ${RTMR_NTHREADS} Proof threads each"
	else
		timeMsg "Beginning test: ${RTMR_NINSTANCES} simultaneous instances with 1 Root thread each"
	fi
	for InstanceID in $(seq 1 $RTMR_NINSTANCES);do
		# launch tests in background
		(
			INSTANCELOGFILE="${INITPWD}/timing.T${InstanceID}.R${REPEAT}.out"
			INSTANCEWORKDIR="${INITPWD}/testCore/T${InstanceID}"
			(
				cd "$INSTANCEWORKDIR"
				`which time` -o "$INSTANCELOGFILE" -f "--Time--\nWall : %e s\nUser : %U s\nSys  : %S s\nCPU  : %P\n--Memory--\nMax RSS : %M KB\nAvr RSS : %t KB\nTot Mem : %K KB\n--Prog--\nCmd  : %C\nExit : %x\n" root -b RunMuSelector.C 1>rootTestROOT.log 2>&1 
			)
			RETVAL=$?
			if [[ $RETVAL -ne 0 ]];then
				warnMsg "Instance ${InstanceID} failed with $RETVAL"
				cat "$INSTANCEWORKDIR/rootTestROOT.log" >&2
				rm "$INSTANCEWORKDIR/rootTestROOT.log"
				touch $RTMR_FAILFILE
				exit $RETVAL
			else
				echoMsg "Instance ${InstanceID} exited with $RETVAL"
			fi
			# get statistics
			WALLTIME=$(grep "Wall :" $INSTANCELOGFILE | awk '{print $3}')
			USERTIME=$(grep "User :" $INSTANCELOGFILE | awk '{print $3}')
			SYSTIME=$(grep "Sys  :" $INSTANCELOGFILE | awk '{print $3}')
			CPUTIME=$(grep "CPU  :" $INSTANCELOGFILE | awk '{print $3}')
			DATASIZE=$(du -s --block-size=K "$INSTANCEWORKDIR/sample/" | awk '{print $1}')
			DATASIZE=${DATASIZE%K}
			rm $INSTANCELOGFILE
			# derive statistics
			KRATE=$(awk "BEGIN{print $DATASIZE / $WALLTIME }")
			if [ -n "$RTMR_NTHREADS" ]
			then
				NICKNAME="P${RTMR_NTHREADS}x${RTMR_NINSTANCES}"
				FULLNAME="I${InstanceID}@${NICKNAME}"
				CKRATE=$(awk "BEGIN{print $DATASIZE / ( $WALLTIME * $RTMR_NTHREADS ) }")
			else
				NICKNAME="R1x${RTMR_NINSTANCES}"
				FULLNAME="I${InstanceID}@${NICKNAME}"
				CKRATE=$KRATE
			fi
			# output and store statistics
			success "() $NICKNAME	 $KRATE KB/S	$CKRATE KB/S/C	[ D:${DATASIZE}  	W:${WALLTIME}  	U:${USERTIME}  	S:${SYSTIME}	C:${CPUTIME}	@:$(hostname)  	I:$RTMR_ID ]"
			echo -e "\t\"${NICKNAME}[$(date '+%y%m%d.%H%M%S')]\" : {
\t\t\"fullname\"  : \"${FULLNAME}\",
\t\t\"nickname\"  : \"${NICKNAME}\",
\t\t\"threads\"   : \"${RTMR_NTHREADS:-1}\",
\t\t\"instances\" : \"${RTMR_NINSTANCES}\",
\t\t\"ID\"        : \"${RTMR_ID}\",
\t\t\"hostname\"  : \"$(hostname)\",
\t\t\"workdir\"   : \"${INITPWD}\",
\t\t\"sampledir\" : \"${RTMR_SAMPLEDIR}\",
\t\t\"cachedrop\" : \"${RTMR_CACHEDROP}\",

\t\t\"samplepass\" : \"${REPEAT}\",

\t\t\"kbps\"      : \"${KRATE}\",
\t\t\"kbpspc\"    : \"${CKRATE}\",

\t\t\"datasize\"  : \"${DATASIZE}\",
\t\t\"walltime\"  : \"${WALLTIME}\",
\t\t\"usertime\"  : \"${USERTIME}\",
\t\t\"systime\"   : \"${SYSTIME}\",
\t\t\"cpupct\"    : \"${CPUTIME}\"
\t}," >> "${RTMR_TIMEFILE}_${InstanceID}"
		) &
	done
	# wait for tests to finish
	wait
	timeMsg "All instances have exited..."
	# save data only if no errors occured
	if [ -f "$RTMR_FAILFILE" ]
	then
		fail "At least one instance exited abnormally"
		echoMsg "Ignoring Data"
	else
		success "All instances exited successfully"
		echoMsg "Storing Data"
		# pass instance log data sequentially to main log to avoid collisions
		for InstanceID in $(seq 1 $RTMR_NINSTANCES);do
			cat "${RTMR_TIMEFILE}_${InstanceID}" >> "${RTMR_TIMEFILE}"
		done
	fi
	for InstanceID in $(seq 1 $RTMR_NINSTANCES);do
		rm "${RTMR_TIMEFILE}_${InstanceID}" 2>/dev/null
	done
)
done

if [ -s "$RTMR_TIMEFILE" ]; then
	cat $RTMR_TIMEFILE >> $RTMR_LOG
	success "Saving data of this run"
	exit 0
else
	success "No valid data in this run"
	exit 1
fi

