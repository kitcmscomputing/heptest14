#!/bin/bash

for shFile in rootTestPrimer*.sh utility.sh
do
	source $shFile
done

# trap for cleanup on exit
cleanUp () {
	[ -n "$RTR_SAMPLEDIR/RTRSAMPLE_${MYRANDOM}" ] && [ -d $RTR_SAMPLEDIR/RTRSAMPLE_${MYRANDOM} ] && ( rm -r $RTR_SAMPLEDIR/RTRSAMPLE_${MYRANDOM}; report "Removing external sample folder" )
	#[ -d testCore/sample ] && ( rm -r testCore/sample; report "Removing sample folder" )
	[ -d testCore ] && ( rm -r testCore; report "Removing test folder" )
	[ -f timing.out ] && ( rm timing.out; report "Removing log files" )
}
trap cleanUp EXIT

# set defaults
RTR_VERBOSITY=${RTR_VERBOSITY:-"0"}
RTR_OUTFILE=${RTR_OUTFILE:-"rootTestStats.out"}
RTR_THREADS=${RTR_THREADS:-"$rootTestProofWorkers"}
RTR_SAMPLEDIR=$RTR_SAMPLEDIR
RTR_ID=${RTR_ID:-"NONE"}

# parse command line options
while [[ $1 ]]
do
	case $1 in
		-h|--help)
			echo "Module for performing a ROOT performance test"
			echo ""
			echo "Implicit file requirement:"
			echo "	rootTestCore.tgz	containing ROOT macro RunMuSelector"
			echo "	rootTestSample*.tgz	containing muon sample"
			echo ""
			echo "	Options:"
			echo "-v|--verbose		increase program verbosity"
			echo '-t|--threads		number of PROOF threads to use'
			echo '					Default: use $rootTestProofWorkers or None (ROOT)'
			echo '-o|--out-file		File to write short format result to'
			exit 0
		;;
		-v|--verbose)
			if [[ $2 == [0-9]* ]]
			then
				CP_VERBOSITY=$[ $CP_VERBOSITY + $2]
				shift 2
			else
				CP_VERBOSITY=$[ $CP_VERBOSITY + 1]
				shift
			fi
		;;
		-i|--identifier)
			RTR_ID=$2
			shift 2
		;;
		-t|--threads)
			RTR_THREADS=$2
			shift 2
		;;
		-s|--sample-destination)
			RTR_SAMPLEDIR=$2
			shift 2
		;;
		-o|--out-file)
			RTR_OUTFILE=$2
			shift 2
		;;
		*)
			echo "Dropping unknown CLI arg: $1"
			shift 1
		;;
	esac
done

# Initialize
MYRANDOM=$RANDOM
STARTDATE=`date +%s`
assertMsg "which root" "Checking for root executable"

# unpack executable
mkdir -p testCore
assertMsg "tar -zxf rootTestCore.tgz -C testCore"		"Unpacking Core"

# unpack sample
if [ -n "$RTR_SAMPLEDIR" ]
then
	mkdir -p "$RTR_SAMPLEDIR/RTRSAMPLE_${MYRANDOM}"
	assertMsg "ln -s '$RTR_SAMPLEDIR/RTRSAMPLE_${MYRANDOM}' testCore/sample"	"Preparing Sample Redirector (dest: $RTR_SAMPLEDIR/RTRSAMPLE_${MYRANDOM})"
else
	mkdir -p testCore/sample
fi
assertMsg "tar -xf rootTestSample*.tar -C testCore/sample"	"Unpacking Sample"
echo ""

# run test
(
	echo "Beginning test"
	cd testCore
	if [ -n "$RTR_THREADS" ]
	then
		export rootTestProofWorkers=$RTR_THREADS
	fi
	`which time` -o ../timing.out -f "--Time--\nWall : %e s\nUser : %U s\nSys  : %S s\nCPU  : %P\n--Memory--\nMax RSS : %M KB\nAvr RSS : %t KB\nTot Mem : %K KB\n--Prog--\nCmd  : %C\nExit : %x\n" root -b RunMuSelector.C 1>rootTestROOT.log 2>&1 
	report "Test"
)
# get statistics
WALLTIME=$(grep "Wall :" timing.out | awk '{print $3}')
USERTIME=$(grep "User :" timing.out | awk '{print $3}')
SYSTIME=$(grep "Sys  :" timing.out | awk '{print $3}')
CPUTIME=$(grep "CPU  :" timing.out | awk '{print $3}')
DATASIZE=$(du -s --block-size=K testCore/sample/ | awk '{print $1}')
DATASIZE=${DATASIZE%K}
# print statistics
echo ""
echo "Stats of this run:	Total $[ $(date +"%s") - $STARTDATE ] s	ID: $RTR_ID"
# cat timing.out
# echo "##################"

KRATE=$(awk "BEGIN{print $DATASIZE / $WALLTIME }")
if [ -n "$RTR_THREADS" ]
then
	echo "Proof: $RTR_THREADS Threads"
	NICKNAME=P$RTR_THREADS
	CKRATE=$(awk "BEGIN{print $DATASIZE / ( $WALLTIME * $RTR_THREADS ) }")
else
	echo "Root:  1 Thread"
	NICKNAME=R1
	CKRATE=$KRATE
fi
echo "Data Size: $(awk "BEGIN{print $DATASIZE / 1024 }") MB	Duration: ${WALLTIME}	User: ${USERTIME}	Sys: ${SYSTIME}	CPU:${CPUTIME}	Host: $(hostname)"
echo "Processing rate:"
echo $(awk "BEGIN{print $DATASIZE / $WALLTIME }") "	KB/s	"
echo $(awk "BEGIN{print $DATASIZE / $WALLTIME / 1024 }") "	MB/s"
echo "$CKRATE 	KB/S/C"
echo ""
echo "() $NICKNAME	 $KRATE KB/S	$CKRATE KB/S/C	[ >${DATASIZE}	D${WALLTIME}	U${USERTIME}	S${SYSTIME}	C${CPUTIME}	@$(hostname):$(pwd)	ID:$RTR_ID]" >> $RTR_OUTFILE

exit 0
