#!/bin/bash

#########
# output formatter
cyan=$(tput setaf 6)
pink=$(tput setaf 5)
blue=$(tput setaf 4)
orange=$(tput setaf 3)
green=$(tput setaf 2)
red=$(tput setaf 1)
bold=$(tput bold)
reset=$(tput sgr0)
#########

export UTIL_PWD=$(dirname ${BASH_SOURCE[0]})
export UTIL_RETCODE_SUCCESS=0
export UTIL_RETCODE_GENERIC=1
export UTIL_RETCODE_PERMISSION=126
export UTIL_RETCODE_NOTFOUND=127

export UTIL_RETCODEUSAGE=2
export UTIL_RETCODEINTERNAL=1

#########
# Pretty MSG
#########
# Return
fail() {
	f_ret=$?
	echo -e "$red${bold}[> Fail <]${reset} $@"
	return $f_ret
}
success() {
	s_ret=$?
	echo -e "$green${bold}[> Done <]${reset} $@"
	return $s_ret
}
# Scripted
timeMsg() {
	echo -e "$pink${bold}[`date "+%H:%M:%S"`]${reset} $@"
}
echoMsg() {
	echo -e "$blue${bold}[> NOTE <]${reset} $@"
}
warnMsg() {
	echo -e "$orange${bold}[> !!!! <]${reset} $@"
}

evaluating() {
	e_ret=$?
	echo -ne "${orange}${bold}[> Eval <]${reset} $@"
	return $e_ret
}
clearline() {
	c_ret=$?
	echo -ne "${reset}\r\033[K"
	return $c_ret
}
#########

#########
# Checks
#########
# Return
report() {
	if [[ $? -eq 0 ]];then
		success $@
	else
		fail $@
	fi
}
assert() {
	eval "$@" 2>/dev/null 1>&2
	ret=$?
	if [[ $ret -eq 0 ]];then
		success $@
	else
		fail $@
	fi
	if [[ $ret -ne 0 ]];then
		exit $ret
	else
		return 0
	fi
}
assertMsg() {
	if [ -z "$2" ]
	then
		Msg=$1
	else
		Msg=$2
	fi
	evaluating $Msg
	eval "$1" 2>/dev/null 1>&2
	ret=$?
	clearline
	if [[ $ret -eq 0 ]];then
		success $Msg
	else
		fail $Msg
	fi
	if [[ $ret -ne 0 ]];then
		exit $ret
	else
		return 0
	fi
}
checkMsg() {
	if [ -z "$2" ]
	then
		Msg=$1
	else
		Msg=$2
	fi
	evaluating $Msg
	eval "$1" 2>/dev/null 1>&2
	ret=$?
	clearline
	if [[ $ne -ne 0 ]];then
		fail $Msg
	fi
	return $ret
}
#########


#########
# Helpers
#########
# CLI converters
util_TrueList=( 'Y' 'y' 'YES' 'Yes' 'yes' 'ON'  'On'  'on'  'TRUE'  'True'  'true'  )
util_FalseList=( 'N' 'n' 'NO'  'No'  'no'  'OFF' 'Off' 'off' 'FALSE' 'False' 'false' )
util_ArgIsTrue() {
	if [[ $1 ]]; then
		return 0
	fi
	for Element in ${util_TrueList[@]}
	do
		if [[ "$1" == "$Element" ]]; then
			return 0
		fi
	done
	return 1
}
util_ArgIsFalse() {
	if [[ $1 ]]; then
		return 0
	fi
	for Element in ${util_FalseList[@]}
	do
		if [[ "$1" == "$Element" ]]; then
			return 0
		fi
	done
	return 1
}

#########
# Legacy Functions
#########
timestamp() {
	if [[ -z "$STARTDATE" ]];then
		STARTDATE=`date +%s`
	fi
	echo "'${@:-NULL}'='$[ $(date +"%s") - $STARTDATE ]'" >> $utilDir\util.timestamps
	echo "[$(date +"%Y:%m:%d-%k:%M:%S")] $@"
}
timereport() {
	echo "== Timestamps =="
	cat $utilDir\util.timestamps
}
