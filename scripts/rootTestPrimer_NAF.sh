#!/bin/bash

echo "Primer: NAF 2.0"
# try SL6 module first
. /usr/share/Modules/init/bash 2>/dev/null 1>&2
module load root/5.34 2>/dev/null 1>&2
# validate
which root 2>/dev/null 1>&2 && echo "Loaded root via: module" && return 0

# if we are still up, module failed - ini is next
# need the ini function for standard syntax
ini ()
{
        eval "`/afs/desy.de/common/etc/local/ini/ini.pl -b $*`"
}
ini root 2>/dev/null 1>&2
# validate
which root 2>/dev/null 1>&2 && echo "Loaded root via: ini" && return 0

# we are still running, so all methods failed
echo "Failed to load root using any method [module, ini]"
return 1
