#!/bin/bash

for shFile in $(shopt -s nullglob; echo {storageTestPrimer*.sh,utility.sh})
do
	source $shFile
done

#######
# Helper functions
#######
# trap for cleanup on exit
__CLEANLIST_LCL=()
cleanUp () {
	if [[ ${#__CLEANLIST_LCL[@]} -gt "0" ]]
	then
		TMP_i=1
		timeMsg "Removing ${#__CLEANLIST_LCL[@]} files..."
		for RMTHING in ${__CLEANLIST_LCL[@]}
		do
			if ! [[ -e ${RMTHING} ]]; then
				checkMsg "${TMP_i}/${#__CLEANLIST_LCL[@]} : File no longer exists"
			elif [[ -d ${RMTHING} ]]; then
				( checkMsg "rm -r ${RMTHING}" "${TMP_i}/${#__CLEANLIST_LCL[@]} : Removing directory" )  || ( warnMsg "Could not remove ${RMTHING}." )
			elif [[ -f ${RMTHING} ]]; then
				( checkMsg "rm    ${RMTHING}" "${TMP_i}/${#__CLEANLIST_LCL[@]} : Removing file" )       || ( warnMsg "Could not remove ${RMTHING}." )
			else
				( warnMsg "${TMP_i}/${#__CLEANLIST_LCL[@]} : Could not remove ${RMTHING}." )
			fi
			TMP_i=$(( $TMP_i + 1 ))
		done
	fi
	timeMsg Exited $MYNAME
}
trap cleanUp EXIT

#######
# Variables and Containers
#######
# internals
InitPwd=$(pwd)
InitDate="$(date '+%y%m%d.%H%M%S')"
MyName=$(basename ${BASH_SOURCE[0]})
MyRANDOM=$RANDOM
# defaults
STMM_TestScript="storageTest_MultiRun.V2.sh"
STMM_NRepeats="1"
STMM_MaxInstances="1"
STMM_Log="log.STRM.${InitDate}.out"

STMM_ScriptArgsList=()
# parse command line options
while [[ $1 ]]
do
	case $1 in
		-h|--help)
			echo "Script for performing a set of storage performance tests"
			echo "   each running multiple storage accesses in parallel"
			echo
			echo "Implicit file requirement:"
			echo "   storageTest_MultiRun.V2.sh test script"
			echo "   utility.sh                 implementation helper scripts"
			echo
			echo "Master Options: [intercept child options]"
			echo '-n|--repeats N                number of complete test repetitions'
			echo '-o|--out-file <FILE>          write results to file <FILE>'
			echo '-m|--max-instances <M>        maximum number of simultaneous processes'
			echo
			echo "Child Options:"
			./$STMM_TestScript -h
			exit 0
		;;
		-o|--out-file)
			STMM_Log=$2
			shift 2
		;;
		-n|--repeats)
			STMM_NRepeats=$2
			shift 2
		;;
		-m|--max-instances)
			STMM_MaxInstances=$2
			shift 2
		;;
		*)
			STMM_ScriptArgsList+=($1)
			shift 1
		;;
	esac
done
STMM_ScriptArgsList+=("-o" $STMM_Log)

timeMsg "Starting script: ${MyName}"

echo -e "{
\"type\"  : \"storageTestMulti\",
\"host\"  : \"$(hostname)\",
\"data\"  : {" > $STMM_Log
echo -e "\t\"__comment\"  : \"R:${STMM_NRepeats} @:$(date '+%y%m%d.%H%M%S')\"" >> $STMM_Log
TMP_j=1
TMP_MaxCount=$(( $STMM_NRepeats * $STMM_MaxInstances ))
for REPEAT in $(seq 1 $STMM_NRepeats)
do
	echo -e "\t,\"__comment\"  : \"R:${REPEAT}/${STMM_NRepeats} @:$(date '+%y%m%d.%H%M%S')\"" >> $STMM_Log
	for InstanceCount in $(seq $STMM_MaxInstances)
	do
		(
		echo -e "\t,\"__comment\"  : \"I:${InstanceCount}/${STMM_MaxInstances} R:${REPEAT}/${STMM_NRepeats} @:$(date '+%y%m%d.%H%M%S')\"" >> $STMM_Log
		STMM_ScriptArgsList+=("-c" $InstanceCount)
		echoMsg "Dispatching: $STMM_TestScript [${TMP_j}/${TMP_MaxCount}]"
		./$STMM_TestScript ${STMM_ScriptArgsList[@]}
		)
		TMP_j=$(( $TMP_j + 1 ))
	done
done
echo -e "}
}
" >> $STMM_Log

timeMsg "Done"
exit 0
