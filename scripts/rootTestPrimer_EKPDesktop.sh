#!/bin/bash

echo "Primer: EKP Desktop"

export ROOTSYS=/usr/users/software/root-versions/5.30.00_64bit
#export ROOTSYS=/usr/users/software/root-versions/5.34.00_64bit
export PATH=$PATH:$ROOTSYS/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ROOTSYS/lib:$ROOTSYS/lib
export PYTHONPATH=$PYTHONPATH:$ROOTSYS/lib:$ROOTSYS/lib/root
