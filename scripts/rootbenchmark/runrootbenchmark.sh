#!/bin/bash

INPUT_CAT=""
TEST_ARGS=()

while [[ $1 ]]
do
	case $1 in
		--input-catalogue)
			echo "> Setting catalogue to $2"
			INPUT_CAT=$2
			shift 2
		;;
		--run-part)
			echo "> Will run part $2"
			RUN_PART=$2
			shift 2
		;;
		*)
			echo "> Appending argument for test: '$1'"
			TEST_ARGS+=($1)
			shift 1
		;;
	esac
done


if [[ -n "${INPUT_CAT}" ]] && [[ -f "${INPUT_CAT}" ]]; then
  # We will ignore other arguments if a catalogue is given
  TEST_ARGS=(--input-catalogue)
  
  # Get a fraction of the catalogue, if requested
  if [[ -n "${RUN_PART}" ]]; then
    if ! echo "${RUN_PART}" | egrep '[[:digit:]]+/[[:digit:]]+' > /dev/null; then
      echo "Please, give part of catalogue to run on"
      echo "in the form n/m meaning part n of m parts"
      exit 1
    fi

    PART=$(echo "${RUN_PART}" | cut -d/ -f1)
    PARTS=$(echo "${RUN_PART}" | cut -d/ -f2)

    _LINES=$(wc -l ${INPUT_CAT} | cut -f1 -d" ")
    _LINES=$(( ${_LINES} / ${PARTS} ))
    head "--lines=$(( ${PART} * ${_LINES}))" ${INPUT_CAT} | tail "--lines=${_LINES}" > ${INPUT_CAT}.${PART}of${PARTS}

    TEST_ARGS+=(${INPUT_CAT}.${PART}of${PARTS})
    echo "> Running on these files:"
    cat ${INPUT_CAT}.${PART}of${PARTS}
  else
    TEST_ARGS+=(${INPUT_CAT})    
  fi
fi


[[ -f "primer.cvmfs.sh" ]] && source primer.cvmfs.sh

chmod a+x ./rootbenchmark

#ldd ./rootbenchmark

echo "" > benchmark.log
echo "{" >> benchmark.log
echo "\"meta\": {" >> benchmark.log
echo "\"hostname\" : \"$(hostname)\"," >> benchmark.log
echo "\"timestamp\" : \"$(date +%s)\"" >> benchmark.log
echo "}," >> benchmark.log
echo "\"benchmark\" : " >> benchmark.log

if [[ $(which time) ]]; 
then
	$(which time) -o "time.log" -f '{"Wall" : "%es",\n"User" : "%Us",\n"Sys"  : "%Ss",\n"CPU"  : "%P",\n"MaxRSS" : "%MKB",\n"AvrRSS" : "%tKB",\n"TotMem" : "%KKB",\n"Cmd"  : "%C",\n"Exit" : "%x" }' ./rootbenchmark ${TEST_ARGS[@]} >> benchmark.log
	retval=$?
else
	echo {} > time.log
	./rootbenchmark ${TEST_ARGS[@]} >> benchmark.log
	retval=$?
fi

echo "}" >> benchmark.log
exit $retval
