#!/bin/bash

[[ ! -x "rootbenchmark" ]] && echo "missing executable" && exit 1
[[ ! -x "runrootbenchmark.sh" ]] && echo "missing control script" && exit 1

shuffle_file_lines() { # shuffle lines of a file
	[[ -z "$1" ]] && { [[ $(which shuf) ]] || [[ $(sort -R "$1") ]];} && return 0
	[[ $(which shuf) ]] && shuf "$1" && return 0
	[[ $(sort -R "$1") ]] && return 0
	return 1
}

INITPWD=$(pwd)
LOG_ID="noid"
MAX_INSTANCES="1"
MIN_INSTANCES="1"
MAX_LOOPS="10"
START_DELAY=0

INPUT_CAT=""
SPLIT_CAT=0
SHUFFEL_CAT=0

TEST_ARGS=()

# parse command line options
while [[ $1 ]]
do
	case $1 in
		-h|--help)
			echo "Script for performing a ROOT performance test"
			echo "   running several ROOT instances in parallel"
			echo ""
			echo "Logging Options:"
			echo '-i|--identifier ID            run identifier ID to assign in logs'
			echo
			echo "Testing Options:"
			echo "-c|--max-count C              max number of ROOT instances to launch [${MAX_INSTANCES}]"
			echo "-m|--min-count M              min number of ROOT instances           [${MIN_INSTANCES}]"
			echo "-l|--loops L                  max number of repetitions              [${MAX_LOOPS}]"
			echo "-d|--delay D                  delay between instance launch          [${START_DELAY}]"
			echo
			echo "Input Options:"
			echo "--input-catalogue IC          file to read input files from          [${INPUT_CAT}]"
			echo "--split-catalogue             split the catalogue for each instance"
			echo "--shuffle-files               randomly read files per instance"
			echo
			echo "Further Options:"
			echo "<see runrootbenchmark.sh>"
			echo "<see rootbenchmark>"
			exit 0
		;;
		-i|--identifier)
			echo "> Setting run ID to $2"
			LOG_ID=$2
			shift 2
		;;
		-c|--max-count)
			echo "> Setting instance count to $2"
			MAX_INSTANCES=$2
			shift 2
		;;
		-m|--min-count)
			echo "> Setting instance count to $2"
			MIN_INSTANCES=$2
			shift 2
		;;
		-l|--loops)
			echo "> Setting loop count to $2"
			MAX_LOOPS=$2
			shift 2
		;;
		-d|--delay)
			echo "> Setting start delay to $2"
			START_DELAY=$2
			shift 2
		;;
		--input-catalogue)
			echo "> Setting catalogue to $2"
			INPUT_CAT=$2
			shift 2
		;;
		--split-catalogue)
			echo "> Splitting catalogue: ON"
			SPLIT_CAT=1
			shift 1
		;;
		--shuffle-files)
			echo "> Shuffling catalogue: ON"
			SHUFFEL_CAT=1
			shift 1
		;;
		*)
			echo "> Appending argument for test: '$1'"
			TEST_ARGS+=($1)
			shift 1
		;;
	esac
done

[[ -z "${INPUT_CAT}" ]] && echo "No input catalogue specified" && exit 2
[[ ${SHUFFEL_CAT} -eq 1 ]] && { shuffle_file_lines /dev/null || { echo "Shuffling requested, but not available"; exit 2 ;} ;}

mkdir -p logs

# temp variables
TMP_CAT=${INITPWD}/tmp.cat

for LOOPRUN in $(seq 1 ${MAX_LOOPS})
do
echo
echo "###+++++++++++++++++++++++++++++++++++++###"
echo "Performing loop ${LOOPRUN}/${MAX_LOOPS}"
echo "###+++++++++++++++++++++++++++++++++++++###"
echo
# iterate max instances
for NOW_INSTANCES in $(seq ${MIN_INSTANCES:-1} ${MAX_INSTANCES:-1})
do
	echo
	echo "###+++++++++++++++++++++++++++++++++++++###"
	echo "Starting ${NOW_INSTANCES} root instances..."
	echo "###+++++++++++++++++++++++++++++++++++++###"
	# catalogue mangling
	cp "${INPUT_CAT}" ${TMP_CAT}
	[[ ${SHUFFEL_CAT} -eq 1 ]] && shuffle_file_lines "${INPUT_CAT}" > "${TMP_CAT}"
	# instance startup
	TSTAMP=$(date +%s)
	for THIS_INSTANCE in $(seq 1 ${NOW_INSTANCES})
	do
		echo "> Starting instance ${THIS_INSTANCE}..."
		WKDIR="${INITPWD}/wkdir.${THIS_INSTANCE}"
		mkdir -p $WKDIR
		# catalogue splitting
		MY_CAT="${WKDIR}/input.${THIS_INSTANCE}.cat"
		if [[ ${SPLIT_CAT} -eq 1 ]]
		then
			_LINES=$(wc -l ${TMP_CAT} | cut -f1 -d" ")
			_LINES=$(( ${_LINES} / ${NOW_INSTANCES} ))
			head "--lines=$(( ${THIS_INSTANCE} * ${_LINES}))" ${TMP_CAT} | tail "--lines=${_LINES}" > ${MY_CAT}
		else
			cp ${TMP_CAT} ${MY_CAT}
		fi
		# instantiation
		(
			cp rootbenchmark runrootbenchmark.sh ${WKDIR}/
			cd $WKDIR
			./runrootbenchmark.sh --input-catalogue ${MY_CAT} ${TEST_ARGS[@]} && echo ">> Finished instance ${THIS_INSTANCE}" && exit 0
			echo '/!\' "Failed instace ${THIS_INSTANCE}!"
			touch ../fail.${NOW_INSTANCES}.msg
		)&
		[[ ${START_DELAY} -gt 0 ]] && [[ ${THIS_INSTANCE} -lt ${NOW_INSTANCES} ]] && echo "Delaying next instance for ${START_DELAY}s" && sleep ${START_DELAY}
	done
	echo "? Started all instances, waiting for completion."
	wait
	if [[ -f "fail.${NOW_INSTANCES}.msg" ]]
	then
		echo "! At least one instance failed."
		echo "Discarding data."
		rm -f "fail.${NOW_INSTANCES}.msg"
	else
		echo "! All instances finished successfully"
		# fetch logs
		for THIS_INSTANCE in $(seq 1 ${NOW_INSTANCES:-1})
		do
			cp "${INITPWD}/wkdir.${THIS_INSTANCE}/benchmark.log" "${INITPWD}/logs/${LOG_ID}.benchmark.${TSTAMP}.${NOW_INSTANCES}.${THIS_INSTANCE}.msg"
			cp "${INITPWD}/wkdir.${THIS_INSTANCE}/time.log" "${INITPWD}/logs/${LOG_ID}.time.${TSTAMP}.${NOW_INSTANCES}.${THIS_INSTANCE}.msg"
		done
		echo "Saved logs to '${INITPWD}/logs/'"
	fi
	echo "Cleaning up workdirs..."
	rm ${TMP_CAT}
	for THIS_INSTANCE in $(seq 1 ${NOW_INSTANCES:-1})
	do
		rm -rf "${INITPWD}/wkdir.${THIS_INSTANCE}"
	done
	echo "Finished run for ${NOW_INSTANCES} instances"
done
done

echo "Finished test run"
echo "###+++++++++++++++++++++++++++++++++++++###"



