#!/bin/bash

if [[ $(uname -r) =~ el5 ]];
then
	source /cvmfs/sft.cern.ch/lcg/external/gcc/4.6.3/x86_64-slc5/setup.sh ""
	source /cvmfs/sft.cern.ch/lcg/external/ROOT/5.34.00/x86_64-slc5-gcc46-opt/root/bin/thisroot.sh
elif [[ $(uname -r) =~ el6 ]];
then
  source /cvmfs/sft.cern.ch/lcg/external/gcc/4.8.1/x86_64-slc6/setup.sh ""
  source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/5.34.13/x86_64-slc6-gcc48-opt/root/bin/thisroot.sh
fi

