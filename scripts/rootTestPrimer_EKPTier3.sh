#!/bin/bash

echo "Primer: EKP Tier 3"

export ROOTSYS=/wlcg/sw/root/5.32.00/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ROOTSYS/lib:$ROOTSYS/lib/root
export PATH=$PATH:$ROOTSYS/bin
export PYTHONPATH=$PYTHONPATH:$ROOTSYS/lib:$ROOTSYS/lib/root

export PATH=$PATH:/usr/users/mfischer/bin