#!/bin/bash

for shFile in utility.sh
do
	source $shFile
done
# trap for cleanup on exit
cleanUp () {
	echo exiting...
	killall rootTest_SingleRun.sh
	[ -d $(pwd)/RTR_Session1 ] && ( rm -r $(pwd)/RTR_Session1; report "Removing RTR_Session1/" )
	echo "Done"
}
trap cleanUp SIGINT EXIT

RTR_M_MAXTHREADS=${RTR_M_MAXTHREADS:-2}
RTR_M_OUTFILE=${RTR_M_OUTFILE:-"rootTest_AutoTest.out"}
RTR_M_REPEAT=${RTR_M_REPEAT:-3}

assertMsg "[ -x rootTest_SingleRun.sh ]"		"Test script exists"
assertMsg "[ -f rootTestCore.tgz ]"	"Test macro exists"
assertMsg "[ -f rootTestSample*.tar ]"	"Test sample exists"

while [[ $1 ]]
do
	case $1 in
		-h|--help)
			echo "Module for performing a ROOT performance test"
			echo ""
			echo "Implicit file requirement:"
			echo "	rootTestCore.tgz	containing ROOT macro RunMuSelector"
			echo "	rootTestSample*.tgz	containing muon sample"
			echo ""
			echo "	Options:"
			echo "-v|--verbose [V]				increase program verbosity [by V]"
			echo '-i|--identifier ID			run identifier ID to assign in logs'
			echo '-o|--out-file FILE			file FILE to write results to'
			echo
			echo '-s|--sample-destination DEST	place samples in subfolder of DEST for each test'
			echo '-n|--repeat N					repeat test N times'
			echo '-t|--max-threads M			max number of parallel sessions and threads to use'
			exit 0
		;;
		-v|--verbose)
			if [[ $2 == [0-9]* ]]
			then
				CP_VERBOSITY=$[ $CP_VERBOSITY + $2]
				shift 2
			else
				CP_VERBOSITY=$[ $CP_VERBOSITY + 1]
				shift
			fi
		;;
		-i|--identifier)
			RTR_M_ID=$2
			shift 2
		;;
		-t|--max-threads)
			RTR_M_MAXTHREADS=$2
			shift 2
		;;
		-s|--sample-destination)
			RTR_M_SAMPLEDIR=$2
			shift 2
		;;
		-n|--repeat)
			RTR_M_REPEAT=$2
			shift 2
		;;
		-o|--out-file)
			RTR_M_OUTFILE=$(basename $2)
			shift 2
		;;
		*)
			echo "Dropping unknown CLI arg: $1"
			shift 1
		;;
	esac
done

# resolve settings
RTR_M_OUTFILE=$(pwd)/$RTR_M_OUTFILE
if [ "$RTR_M_MAXTHREADS" = "auto" ]; then
	RTR_M_MAXTHREADS=$(nproc)
fi
echo "Master for ROOT performance tests"
echo "Writing to:"
echo "	$RTR_M_OUTFILE"
echo "Will perform following tests:"
if [ "$RTR_M_MAXTHREADS" -gt "1" ]; then
	echo "	ROOT	: 1-$RTR_M_MAXTHREADS sessions"
	echo "	PROOF	: 1-$RTR_M_MAXTHREADS workers x 1-$RTR_M_MAXTHREADS sessions"
else
	echo "	ROOT	: 1 session"
	echo "	PROOF	: 1 workers x 1 session"
fi
echo ""

echo "----------"
echo "Testing..."
echo "----------"
MAX_SESS=$RTR_M_MAXTHREADS
THIS_SESS=$MAX_SESS

# construct SingelRun arguments
SINGLERUNARG="-o $RTR_M_OUTFILE"
if [ -n "$RTR_M_ID" ]
then
	SINGLERUNARG+=" -i $RTR_M_ID"
fi
if [ -n "$RTR_M_SAMPLEDIR" ]
then
 	SINGLERUNARG+=" -s $RTR_M_SAMPLEDIR"
fi

echo "[################]" > $RTR_M_OUTFILE
echo "{} S$MAX_SESS" >> $RTR_M_OUTFILE
for ITER in $(seq 1 $RTR_M_REPEAT);do
	echo "{} T$(date '+%s')	[$(date '+%Y-%m-%d+%H:%M:%S')]" >> $RTR_M_OUTFILE
		echo "ROOT"
		(
		SESSION_FOLDER=$(pwd)/RTR_Session${THIS_SESS}R1
		mkdir -p $SESSION_FOLDER
		cp utility.sh rootTest_SingleRun.sh rootTestCore.tgz rootTestPrimer*.sh rootTestSample*.tar $SESSION_FOLDER
		cd $SESSION_FOLDER
		./rootTest_SingleRun.sh $SINGLERUNARG
		cd ..
		assertMsg "rm -r $SESSION_FOLDER" "Removing test folder..."
		)
	for THIS_THREAD in $(seq 1 $RTR_M_MAXTHREADS); do
		echo "Proof: $THIS_THREAD"
		(
		SESSION_FOLDER=$(pwd)/RTR_Session${THIS_SESS}P${THIS_THREAD}
		mkdir -p $SESSION_FOLDER
		cp utility.sh rootTest_SingleRun.sh rootTestCore.tgz rootTestPrimer*.sh rootTestSample*.tar $SESSION_FOLDER
		cd $SESSION_FOLDER
		./rootTest_SingleRun.sh -t $THIS_THREAD $SINGLERUNARG
		cd ..
		assertMsg "rm -r $SESSION_FOLDER" "Removing test folder..."
		)
	done
	echo "{} T$(date '+%s')	[$(date '+%Y-%m-%d+%H:%M:%S')]" >> $RTR_M_OUTFILE
	echo "[################]" >> $RTR_M_OUTFILE
done